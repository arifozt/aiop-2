
export const sourcesList = [
    {
        "name": "SPOTIFY",
        "img": "https://www.freepnglogos.com/uploads/spotify-logo-png/image-gallery-spotify-logo-21.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#1ED45F",
        "selected": false
    },
    {
        "name": "REDDIT",
        "img": "https://www.redditinc.com/assets/images/site/reddit-logo.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#FF4300",
        "selected": false
    },
    {
        "name": "YOUTUBE",
        "img": "http://assets.stickpng.com/images/580b57fcd9996e24bc43c545.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#ff0000",
        "selected": false
    },
    {
        "name": "TWITCH",
        "img": "http://assets.stickpng.com/thumbs/580b57fcd9996e24bc43c540.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#6441a5",
        "selected": false
    },
    {
        "name": "TIKTOK",
        "img": "https://www.designbust.com/download/1329/png/transparent_tiktok_logo256.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#00f7ef",
        "selected": false
    },
    {
        "name": "WIKIPEDIA",
        "img": "https://upload.wikimedia.org/wikipedia/commons/6/63/Wikipedia-logo.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#808080",
        "selected": false
    },
    {
        "name": "TWITTER",
        "img": "http://assets.stickpng.com/images/580b57fcd9996e24bc43c53e.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#00acee",
        "selected": false
    },
    {
        "name": "BBC",
        "img": "http://assets.stickpng.com/images/5842ab62a6515b1e0ad75b09.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#000000",
        "selected": false
    },
    {
      "name": "CNN",
      "img": "http://assets.stickpng.com/images/5842ab75a6515b1e0ad75b0b.png",
      "icon": "A",
      "sourceLink": "aaaaa",
      "color": "#C60000",
      "selected": false
    },
    {
        "name": "YAHOO",
        "img": "https://www.freepnglogos.com/uploads/yahoo-logo-png/yahoo-logo-png-free-download-3.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#5F01D1",
        "selected": false
    },
    {
        "name": "BUZZFEED",
        "img": "https://www.buzzfeed.com/static-assets/img/buzzfeed_arrow.e86a786d9e5e2250e1ed3e0ec95ba42d.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#ED3624",
        "selected": false
    },
    {
        "name": "BUSINESSINSIDER",
        "img": "http://media.insider.com/public/assets/BI/US/logos/logos-page/BI_light_background_color_vertical.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#185F7D",
        "selected": false
    },
    {
        "name": "WASHINGTONPOST",
        "img": "https://www.thewrap.com/wp-content/uploads/2020/07/Washington-Post-logo-618x400.jpg",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#FFFFFF",
        "selected": false
    },
    {
        "name": "BUSINESS-STANDARD",
        "img": "https://coreviewsystems.com/wp-content/uploads/Business-Standard-logo-2-e1570433327679.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#A72021",
        "selected": false
    },
    {
        "name": "CNBC",
        "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/CNBC_logo.svg/1280px-CNBC_logo.svg.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#FCB711",
        "selected": false
    },
    {
        "name": "GUARDIAN",
        "img": "https://doctorswith.me/wp-content/uploads/The-Guardian-Logo-2005.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#00417B",
        "selected": false
    },
    {
        "name": "DW",
        "img": "https://upload.wikimedia.org/wikipedia/commons/d/d1/DW_Logo_2012.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#012D5A",
        "selected": false
    },
    {
        "name": "CHANNELNEWSASIA",
        "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Channel_NewsAsia_logo_%28shape_only%29.svg/1123px-Channel_NewsAsia_logo_%28shape_only%29.svg.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#EF1520",
        "selected": false
    },
    {
        "name": "BANGKOKPOST",
        "img": "https://logos-download.com/wp-content/uploads/2016/05/Bangkok_Post_logo_blue_wordmark.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#213C70",
        "selected": false
    },
    {
        "name": "COMMONDREAMS",
        "img": "https://www.commondreams.org/themes/custom/commondreams_theme/logo.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#067A0B",
        "selected": false
    },
    {
        "name": "THENATIONALNEWS",
        "img": "https://upload.wikimedia.org/wikipedia/en/6/66/The_National_Newspaper_Logo.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#004E79",
        "selected": false
    },
    {
        "name": "CITYAM",
        "img": "https://batonsystems.com/wp-content/uploads/2022/01/City-AM-Optimised-Logo.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#009DDC",
        "selected": false
    },
    {
        "name": "DAILYHIVE",
        "img": "https://assets.dailyhive.com/assets/2.3.6/static/svg/logos/dailyhive-color.svg",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#14CCDC",
        "selected": false
    },
    {
        "name": "YNETNEWS",
        "img": "https://www.ynetnews.com/images/ynetnewsresp/logo-menu.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#FF1200",
        "selected": false
    },
    {
        "name": "APPLEINSIDER",
        "img": "https://www.logolynx.com/images/logolynx/72/72606317f6206eea4de86b7d35584e9c.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#6A737E",
        "selected": false
    },
    {
        "name": "THEGLOBEANDMAIL",
        "img": "https://cdn.freelogovectors.net/wp-content/uploads/2021/02/the-globe-and-mail-logo-freelogovectors.net_.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#CC2F2C",
        "selected": false
    },
    {
        "name": "THESCOTTISHSUN",
        "img": "https://cached.imagescaler.hbpl.co.uk/resize/scaleWidth/743/cached.offlinehbpl.hbpl.co.uk/news/OMC/D4324EA1-0BC8-D6A2-1374162B769E61DB.jpg",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#FB0024",
        "selected": false
    },
    {
        "name": "THESUN",
        "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3b/The_Sun.svg/1024px-The_Sun.svg.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#EC1000",
        "selected": false
    },
    {
        "name": "THETELEGRAPHANDARGUS",
        "img": "http://www.thetelegraphandargus.co.uk/resources/images/sitelogo/",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#283583",
        "selected": false
    },
    {
        "name": "THETIMES",
        "img": "https://seeklogo.com/images/T/The_Times_100-logo-1294566530-seeklogo.com.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#AAAAAA",
        "selected": false
    },
    {
        "name": "USNEWS",
        "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/U.S._News_%26_World_Report_logo.svg/1280px-U.S._News_%26_World_Report_logo.svg.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#005EA7",
        "selected": false
    },
    {
        "name": "VICE",
        "img": "http://assets.stickpng.com/images/594666b232f10292497d89c6.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#AAAAAA",
        "selected": false
    },
    {
        "name": "RTE",
        "img": "https://brandslogos.com/wp-content/uploads/thumbs/rte-logo-vector.svg",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#1A3B73",
        "selected": false
    },
    {
        "name": "SALTWIRE",
        "img": "https://www.saltwire.com/static/img/sw-button-sea.1859f266b84e.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#444C62",
        "selected": false
    },
    {
        "name": "SCMP",
        "img": "https://corp.scmp.com/wp-content/uploads/2018/02/SCMP_logo_03.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#001B54",
        "selected": false
    },
    {
        "name": "SCOTSMAN",
        "img": "https://www.vectorcloud.co.uk/wp-content/uploads/2018/06/the-scotsman-logo.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#FFFFFF",
        "selected": false
    },
    {
        "name": "SKY",
        "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/d/de/Sky-news-logo.svg/2560px-Sky-news-logo.svg.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#AC0000",
        "selected": false
    },
    {
        "name": "SMITHSONIANMAG",
        "img": "https://www.typeinvestigations.org/wp-content/uploads/2018/11/smithsonian.mag_.logo_.jpg",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#FFFFFF",
        "selected": false
    },
    {
        "name": "STRIPES",
        "img": "https://logos-world.net/wp-content/uploads/2021/03/Stripe-Symbol.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#665BFF",
        "selected": false
    },
    {
        "name": "NDTV",
        "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/NDTV_logo.svg/1200px-NDTV_logo.svg.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#EE0000",
        "selected": false
    },
    {
        "name": "THEAVIATIONIST",
        "img": "https://pbs.twimg.com/profile_images/1440166402812383234/EVurpfGH_400x400.jpg",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#FEF302",
        "selected": false
    },
    {
        "name": "THEDAILYBEAST",
        "img": "https://talkingbiznews.com/wp-content/uploads/2021/03/171117141704-the-daily-beast-640x360-1.jpg",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#D00000",
        "selected": false
    },
    {
        "name": "THEDRIVE",
        "img": "https://tribunecontentagency.com/wp-content/uploads/2019/10/logo-Drive_The-2019-rgb.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#F8A341",
        "selected": false
    },
    {
        "name": "DAILYSABAH",
        "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/Daily_Sabah_logo.svg/2560px-Daily_Sabah_logo.svg.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#343333",
        "selected": false
    },
    {
        "name": "ELPAIS",
        "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/El_Pais_logo_2007.svg/2560px-El_Pais_logo_2007.svg.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#01699D",
        "selected": false
    },
    {
        "name": "AL-MONITOR",
        "img": "https://i.pinimg.com/474x/53/31/83/5331831a1fe84ad6ccf3f448ba0cadd5.jpg",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#0076C0",
        "selected": false
    },
    {
        "name": "ALGEMEINER",
        "img": "https://res.cloudinary.com/crunchbase-production/image/upload/c_lpad,f_auto,q_auto:eco,dpr_1/v1436520650/jsgsu4nmuammis9y6sej.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#49494D",
        "selected": false
    },
    {
        "name": "APNEWS",
        "img": "https://image.pngaaa.com/8/993008-middle.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#C25437",
        "selected": false
    },
    {
        "name": "CBSNEWS",
        "img": "https://www.vhv.rs/dpng/d/33-338000_cbs-news-logo-png-transparent-png.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#000000",
        "selected": false
    },
    {
        "name": "DAILYMAIL",
        "img": "https://image.pngaaa.com/480/3881480-middle.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#000000",
        "selected": false
    },
    {
        "name": "THEHILL",
        "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/The_Hill_%282020-01-15%29.svg/800px-The_Hill_%282020-01-15%29.svg.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#0676C0",
        "selected": false
    },
    {
        "name": "THEMOSCOWTIMES",
        "img": "https://media.glassdoor.com/sqll/329118/the-moscow-times-squarelogo-1428496547592.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#286290",
        "selected": false
    },
    {
        "name": "URDUPOINT",
        "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cb/UrduPoint_Logo.png/640px-UrduPoint_Logo.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#GGGGGG",
        "selected": false
    },
    {
        "name": "BBC",
        "img": "http://assets.stickpng.com/images/5842ab62a6515b1e0ad75b09.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#000000",
        "selected": false
    },
    {
        "name": "KENTONLINE",
        "img": "https://www.kentonline.co.uk/group-logo.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#FFDD00",
        "selected": false
    },
    {
        "name": "RT",
        "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Russia-Today-Logo.svg/1280px-Russia-Today-Logo.svg.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#B6D74A",
        "selected": false
    },
    {
        "name": "INDIATIMES",
        "img": "https://static.wikia.nocookie.net/logopedia/images/d/d5/Indiatimes_stacked_logo.png/revision/latest?cb=20210603150713",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#ED244F",
        "selected": false
    },
    {
        "name": "DAILYRECORD",
        "img": "https://upload.wikimedia.org/wikipedia/en/thumb/3/3d/Daily_Record_logo.svg/1200px-Daily_Record_logo.svg.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#EF3D43",
        "selected": false
    },
    {
        "name": "FORBES",
        "img": "https://www.freepnglogos.com/uploads/forbes-logo-png/logo-forbes-png-transparent-11.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#FFFFFF",
        "selected": false
    },
    {
        "name": "DEVONLIVE",
        "img": "https://s2-prod.devonlive.com/@trinitymirrordigital/chameleon-branding/publications/devonlive/img/logo-devonlive.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#D90000",
        "selected": false
    },
    {
        "name": "EKATHIMERINI",
        "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Logo-kathimerini-gr.svg/1200px-Logo-kathimerini-gr.svg.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#3878A7",
        "selected": false
    },
    {
        "name": "ENTERTAINMENTDAILY",
        "img": "https://cdn.entertainmentdaily.com/2018/03/14172345/ED-300x300.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#D70A51",
        "selected": false
    },
    {
        "name": "EUOBSERVER",
        "img": "https://euobserver.com/graphics/logo.svg",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#E57747",
        "selected": false
    },
    {
        "name": "EURACTIV",
        "img": "http://www.euractiv.com/wp-content/uploads/sites/2/2017/02/EURACTIV_LOGO_ORIGINAL_RGB_XL-1.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#FFC709",
        "selected": false
    },
    {
        "name": "EURONEWS",
        "img": "https://upload.wikimedia.org/wikipedia/commons/3/39/Euronews._2016_alternative_logo.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#033766",
        "selected": false
    },
    {
        "name": "EXAMINERLIVE",
        "img": "https://s2-prod.examinerlive.co.uk/@trinitymirrordigital/chameleon-branding/publications/huddersfieldexaminer/img/logo-huddersfieldexaminer.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#4E7FFF",
        "selected": false
    },
    {
        "name": "EXPRESS",
        "img": "https://cdn.freebiesupply.com/logos/thumbs/2x/express-fashion-stores-logo.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#000000",
        "selected": false
    },
    {
        "name": "FOXNEWS",
        "img": "https://logos-world.net/wp-content/uploads/2020/11/Fox-News-Channel-Emblem.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#003366",
        "selected": false
    },
    {
        "name": "FRANCE24",
        "img": "https://cdn.freelogovectors.net/wp-content/uploads/2021/08/france-24-logo-freelogovectors.net_.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#1CA0C7",
        "selected": false
    },
    {
        "name": "FT",
        "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/Financial_Times_corporate_logo_%28no_background%29.svg/1200px-Financial_Times_corporate_logo_%28no_background%29.svg.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#FCD1B0",
        "selected": false
    },
    {
        "name": "GAROWEONLINE",
        "img": "https://upload.wikimedia.org/wikipedia/commons/7/7a/Garowe_Online_logo.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#062963",
        "selected": false
    },
    {
        "name": "GLOBALVOICES",
        "img": "https://s3.amazonaws.com/static.globalvoices/img/logos/gv-logo-2014-horizontal-6000.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#269926",
        "selected": false
    },
    {
        "name": "GOOGLE",
        "img": "http://assets.stickpng.com/images/580b57fcd9996e24bc43c51f.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#4285F4",
        "selected": false
    },
    {
        "name": "HAARETZ",
        "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/Haaretz_en.svg/2560px-Haaretz_en.svg.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#000000",
        "selected": false
    },
    {
        "name": "HULLDAILYMAIL",
        "img": "https://www.newpathsmusic.com/wp-content/uploads/sites/23/2019/04/Hull-Daily-Mail-logo.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#991B20",
        "selected": false
    },
    {
        "name": "REUTERS",
        "img": "http://assets.stickpng.com/images/5848252ccef1014c0b5e49d1.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#FF8000",
        "selected": false
    },
    {
        "name": "INEWS",
        "img": "https://upload.wikimedia.org/wikipedia/en/thumb/f/f5/INews_TV_logo.svg/1200px-INews_TV_logo.svg.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#ED1C24",
        "selected": false
    },
    {
        "name": "INSIDERPAPER",
        "img": "https://media.gettr.com/group43/origin/2021/07/01/17/b0fcc225-4a0a-be23-4216-a37aec6a16f5/0ed381b4dcad3ba04936ff0331a36dcc_500x0.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#419A1C",
        "selected": false
    },
    {
        "name": "MYLONDON",
        "img": "https://s2-prod.mylondon.news/@trinitymirrordigital/chameleon-branding/publications/getwestlondon/img/logo-getwestlondon.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#DC241F",
        "selected": false
    },
    {
        "name": "IRISHTIMES",
        "img": "https://www.logolynx.com/images/logolynx/02/0207ae23a0b541924518f6c967cd2248.jpeg",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#FFFFFF",
        "selected": false
    },
    {
        "name": "JAMAICA-GLEANER",
        "img": "https://logodix.com/logo/1821806.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#005AAB",
        "selected": false
    },
    {
        "name": "JPOST",
        "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Jerusalem_Post_Logo.svg/1280px-Jerusalem_Post_Logo.svg.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#B61319",
        "selected": false
    },
    {
        "name": "NYTIMES",
        "img": "https://www.pngkey.com/png/full/79-791143_the-new-york-times-new-york-times-logo.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#FFFFFF",
        "selected": false
    },
    {
        "name": "CHRONICLELIVE",
        "img": "https://s2-prod.chroniclelive.co.uk/@trinitymirrordigital/chameleon-branding/publications/nechronicle/img/logo-nechronicle.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#0065B3",
        "selected": false
    },
    {
        "name": "MESONSTARS",
        "img": "https://pbs.twimg.com/profile_images/1482909054804582403/x_XENqtb_400x400.jpg",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#240409",
        "selected": false
    },
    {
        "name": "MONGABAY",
        "img": "https://viz.mongabay.com/src/assets/images/logo.jpg",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#539B09",
        "selected": false
    },
    {
        "name": "LEICESTERMERCURY",
        "img": "https://local.reachsolutions.co.uk/truelocal/assets/img/brands/leicester_mercury_stacked_logo.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#18A4D0",
        "selected": false
    },
    {
        "name": "NASDAQ",
        "img": "https://cdn.freebiesupply.com/logos/thumbs/2x/nasdaq1-logo.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#53BDD5",
        "selected": false
    },
    {
        "name": "NATIONALPOST",
        "img": "https://logos-download.com/wp-content/uploads/2016/05/National_Post_logo.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#FFCC00",
        "selected": false
    },
    {
        "name": "NBCNEWS",
        "img": "https://upload.wikimedia.org/wikipedia/commons/9/9f/NBC_News_2013_logo.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#F89C30",
        "selected": false
    },
    {
        "name": "NCRONLINE",
        "img": "https://www.stmaryoticandhoc.org/uploads/1/3/1/9/131993067/national-catholic-reporter_orig.jpg",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#016AAE",
        "selected": false
    },
    {
        "name": "NEWS",
        "img": "https://logoeps.com/wp-content/uploads/2014/05/21601-news-logo-icon-vector-icon-vector-eps.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#FFFFFF",
        "selected": false
    },
    {
        "name": "NEWSNOW",
        "img": "https://upload.wikimedia.org/wikipedia/commons/thumb/d/de/Newsnow-logo_red.png/480px-Newsnow-logo_red.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#C2002C",
        "selected": false
    },
    {
        "name": "NEWSWEEK",
        "img": "https://cdn.freelogovectors.net/wp-content/uploads/2019/03/newsweeklogo.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#F72210",
        "selected": false
    },
    {
        "name": "NEWZIT",
        "img": "https://www.newzit.com/static/2.0.2/socialImage.jpg",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#64615A",
        "selected": false
    },
    {
        "name": "NOTTINGHAMPOST",
        "img": "https://s2-prod.nottinghampost.com/@trinitymirrordigital/chameleon-branding/publications/nottinghampost/img/logo-nottinghampost.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#17542A",
        "selected": false
    },
    {
        "name": "NPR",
        "img": "https://1000logos.net/wp-content/uploads/2021/05/NPR-logo.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#EE3A23",
        "selected": false
    },
    {
        "name": "RAPPLER",
        "img": "https://upload.wikimedia.org/wikipedia/en/thumb/6/69/Rappler_logo.svg/1200px-Rappler_logo.svg.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#E86224",
        "selected": false
    },
    {
        "name": "RENEWABLESNOW",
        "img": "https://images.squarespace-cdn.com/content/v1/5e8c11945a2b0e04f9a2aad7/1621579682872-3DMT15PN1DY79HV0BDLW/ren-logo-color.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#6EC315",
        "selected": false
    },
    {
        "name": "RFERL",
        "img": "https://www.usagm.gov/wp-content/uploads/2011/11/logo_rferl.png",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#EA6903",
        "selected": false
    },
    {
        "name": "RIOTIMESONLINE",
        "img": "https://pbs.twimg.com/profile_images/1165920488746553344/8GA3uxib_400x400.jpg",
        "icon": "A",
        "sourceLink": "aaaaa",
        "color": "#393536",
        "selected": false
    },
]

// export const sourceImageMap = {
//   "vice" : "https://w7.pngwing.com/pngs/945/407/png-transparent-new-york-city-vice-media-logo-vice-love-text-logo.png",
//   "cnn" : "http://assets.stickpng.com/images/5842ab75a6515b1e0ad75b0b.png",
//   "yahoo" : "https://www.freepnglogos.com/uploads/yahoo-logo-png/yahoo-logo-png-free-download-3.png",
//   "nadaq" : "https://logodownload.org/wp-content/uploads/2019/07/nasdaq-logo.png",
//   "telegraph" : "https://i.dawn.com/primary/2019/10/5db511b4487bd.jpg",
//   "al-monitor" : "https://i.pinimg.com/474x/53/31/83/5331831a1fe84ad6ccf3f448ba0cadd5.jpg",
//   "algemeiner" : "https://res.cloudinary.com/crunchbase-production/image/upload/c_lpad,f_auto,q_auto:eco,dpr_1/v1436520650/jsgsu4nmuammis9y6sej.png",
//   "bangkokpost" : "https://logos-download.com/wp-content/uploads/2016/05/Bangkok_Post_logo_blue_wordmark.png",
//   "business-standard" : "https://coreviewsystems.com/wp-content/uploads/Business-Standard-logo-2-e1570433327679.png",
//   "businessinsider" : "http://media.insider.com/public/assets/BI/US/logos/logos-page/BI_light_background_color_vertical.png",
//   "channelnewsasia" : "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Channel_NewsAsia_logo_%28shape_only%29.svg/1123px-Channel_NewsAsia_logo_%28shape_only%29.svg.png",
//   "cityam" : "https://batonsystems.com/wp-content/uploads/2022/01/City-AM-Optimised-Logo.png",
//   "cnbc" : "https://cdn.freebiesupply.com/images/large/2x/cnbc-logo-transparent.png",
//   "commondreams" : "https://www.commondreams.org/themes/custom/commondreams_theme/logo.png",
//   "dailyhive" : "https://assets.dailyhive.com/assets/2.3.6/static/svg/logos/dailyhive-color.svg",
//   "dailysabah" : "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/Daily_Sabah_logo.svg/2560px-Daily_Sabah_logo.svg.png",
//   "dw" : "https://upload.wikimedia.org/wikipedia/commons/d/d1/DW_Logo_2012.png",
//   "ekathimerini" : "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Logo-kathimerini-gr.svg/1200px-Logo-kathimerini-gr.svg.png",
//   "elpais" : "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/El_Pais_logo_2007.svg/2560px-El_Pais_logo_2007.svg.png",
//   "washingtonpost" : "https://www.thewrap.com/wp-content/uploads/2020/07/Washington-Post-logo-618x400.jpg",
//   "ynetnews" : "https://www.ynetnews.com/images/ynetnewsresp/logo-menu.png",
//   "usnsews" : "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/U.S._News_%26_World_Report_logo.svg/1280px-U.S._News_%26_World_Report_logo.svg.png",
//   "urdupoint" : "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cb/UrduPoint_Logo.png/640px-UrduPoint_Logo.png",
//   "thenationalnews" : "https://upload.wikimedia.org/wikipedia/en/6/66/The_National_Newspaper_Logo.png",
//   "themoscowtimes" : "https://media.glassdoor.com/sqll/329118/the-moscow-times-squarelogo-1428496547592.png",
//   "twitter" : "http://assets.stickpng.com/images/580b57fcd9996e24bc43c53e.png",
//   "buzzfeed" : "https://www.buzzfeed.com/static-assets/img/buzzfeed_arrow.e86a786d9e5e2250e1ed3e0ec95ba42d.png",
//   "bbc" : "http://assets.stickpng.com/images/5842ab62a6515b1e0ad75b09.png",
//   "cnn" : "",
//   "cnn" : "",
//   "cnn" : "",
//   "cnn" : "",
//   "cnn" : "",
//   "cnn" : "",
//   "cnn" : "",
//   "cnn" : "",
//   "cnn" : "",
//   "cnn" : "",
//   "cnn" : "",
//   "cnn" : "",
//   "cnn" : "",
//   "cnn" : "",
//   "cnn" : "",
//   "cnn" : "",
//   "apnews" : "https://image.pngaaa.com/8/993008-middle.png",
//   "appleinsider" : "https://www.logolynx.com/images/logolynx/72/72606317f6206eea4de86b7d35584e9c.png",
//   "cbsnews" : "https://www.vhv.rs/dpng/d/33-338000_cbs-news-logo-png-transparent-png.png",
//   "dailymail" : "https://image.pngaaa.com/480/3881480-middle.png",
//   "thehill": "https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/The_Hill_%282020-01-15%29.svg/800px-The_Hill_%282020-01-15%29.svg.png"
// // }
// export const sourceBorderColorMap = {
//   "vice" : "#003a3f",
//   "cnn" : "#E03E34",
//   "apnews" : "#C25437",
//   "appleinsider" : "#6A737E",
//   "cbsnews" : "#000000",
//   "dailymail" : "#000000",
//   "thehill": "#abaca4",
//   "yahoo" : "#5F01D1",
//   "nasdaq" : "#0087BA",
//   "telegraph" : "#04455B",
//   "algemeiner" : "#49494D",
//   "bangkokpost" : "#213C70",
//   "business-standard" : "#A72021",
//   "businessinsider" : "#185F7D",
//   "channelnewsasia" : "#EF1520",
//   "cityam" : "#009DDC",
//   "cnbc" : "#FCB711",
//   "commondreams" : "#067A0B",
//   "dailyhive" : "#14CCDC",
//   "dailysabah" : "#343333",
//   "dw" : "#012D5A",
//   "ekathimerini" : "#3878A7",
//   "elpais" : "#01699D",
//   "washingtonpost" : "#FFFFFF",
//   "ynetnews" : "#FF1200",
//   "usnews" : "#005EA7",
//   "urdupoint" : "#GGGGGG",
//   "thenationalnews" : "#004E79",
//   "themoscowtimes" : "#286290",
//   "twitter" : "#55ADEE",
//   "buzzfeed" : "#ED3624",
//   "bbc" : "#000000",
//   "buzzfeed" : "#ED3624",
//   "buzzfeed" : "#ED3624",
//   "al-monitor" : "#0076C0",
  
// }



// allSourcesList.push({ name: "SPOTIFY", img: "https://www.freepnglogos.com/uploads/spotify-logo-png/image-gallery-spotify-logo-21.png", icon: "A", sourceLink: "aaaaa", color: "#1ED45F", selected: "false" })
// allSourcesList.push({ name: "REDDIT", img: "https://www.redditinc.com/assets/images/site/reddit-logo.png", icon: "A", sourceLink: "aaaaa", color: "#FF4300", selected: "false" })
// allSourcesList.push({ name: "YOUTUBE", img: "http://assets.stickpng.com/images/580b57fcd9996e24bc43c545.png", icon: "A", sourceLink: "aaaaa", color: "#ff0000", selected: "false" })
// allSourcesList.push({ name: "TWITCH", img: "http://assets.stickpng.com/thumbs/580b57fcd9996e24bc43c540.png", icon: "A", sourceLink: "aaaaa", color: "#6441a5", selected: "false" })
// allSourcesList.push({ name: "TIKTOK", img: "https://www.designbust.com/download/1329/png/transparent_tiktok_logo256.png", icon: "A", sourceLink: "aaaaa", color: "#00f7ef", selected: "false" })
// allSourcesList.push({ name: "WIKIPEDIA", img: "https://upload.wikimedia.org/wikipedia/commons/6/63/Wikipedia-logo.png", icon: "A", sourceLink: "aaaaa", color: "#808080", selected: "false" })
// allSourcesList.push({ name: "TWITTER", img: "http://assets.stickpng.com/images/580b57fcd9996e24bc43c53e.png", icon: "A", sourceLink: "aaaaa", color: "#00acee", selected: "false" })
// allSourcesList.push({ name: "BBC", img: "http://assets.stickpng.com/images/5842ab62a6515b1e0ad75b09.png", icon: "A", sourceLink: "aaaaa", color: "#000000", selected: "false" })
// allSourcesList.push({ name: "GUARDIAN", img: "https://doctorswith.me/wp-content/uploads/The-Guardian-Logo-2005.png", icon: "A", sourceLink: "aaaaa", color: "#00417B", selected: "false" })
// allSourcesList.push({ name: "YAHOO", img: "https://www.freepnglogos.com/uploads/yahoo-logo-png/yahoo-logo-png-free-download-3.png", icon: "A", sourceLink: "aaaaa", color: "#5F01D1", selected: "false" })
// allSourcesList.push({ name: "TELEGRAPH", img: "https://i.dawn.com/primary/2019/10/5db511b4487bd.jpg", icon: "A", sourceLink: "aaaaa", color: "#04455B", selected: "false" })
// allSourcesList.push({ name: "BUZZFEED", img: "https://www.buzzfeed.com/static-assets/img/buzzfeed_arrow.e86a786d9e5e2250e1ed3e0ec95ba42d.png", icon: "A", sourceLink: "aaaaa", color: "#ED3624", selected: "false" })
// allSourcesList.push({ name: "CNN", img: "http://assets.stickpng.com/images/5842ab75a6515b1e0ad75b0b.png", icon: "A", sourceLink: "aaaaa", color: "#C60000", selected: "false" })
// allSourcesList.push({ name: "BUSINESSINSIDER", img: "http://media.insider.com/public/assets/BI/US/logos/logos-page/BI_light_background_color_vertical.png", icon: "A", sourceLink: "aaaaa", color: "#185F7D", selected: "false" })
// allSourcesList.push({ name: "BUSINESS-STANDARD", img: "https://coreviewsystems.com/wp-content/uploads/Business-Standard-logo-2-e1570433327679.png", icon: "A", sourceLink: "aaaaa", color: "#A72021", selected: "false" })
// allSourcesList.push({ name: "CHANNELNEWSASIA", img: "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Channel_NewsAsia_logo_%28shape_only%29.svg/1123px-Channel_NewsAsia_logo_%28shape_only%29.svg.png", icon: "A", sourceLink: "aaaaa", color: "#EF1520", selected: "false" })
// allSourcesList.push({ name: "THENATIONALNEWS", img: "https://upload.wikimedia.org/wikipedia/en/6/66/The_National_Newspaper_Logo.png", icon: "A", sourceLink: "aaaaa", color: "#004E79", selected: "false" })
// allSourcesList.push({ name: "YNETNEWS", img: "https://www.ynetnews.com/images/ynetnewsresp/logo-menu.png", icon: "A", sourceLink: "aaaaa", color: "#00417B", selected: "false" })
// allSourcesList.push({ name: "DW", img: "https://upload.wikimedia.org/wikipedia/commons/d/d1/DW_Logo_2012.png", icon: "A", sourceLink: "aaaaa", color: "#012D5A", selected: "false" })
// allSourcesList.push({ name: "COMMONDREAMS", img: "https://www.commondreams.org/themes/custom/commondreams_theme/logo.png", icon: "A", sourceLink: "aaaaa", color: "#067A0B", selected: "false" })
// allSourcesList.push({ name: "DAILYHIVE", img: "https://assets.dailyhive.com/assets/2.3.6/static/svg/logos/dailyhive-color.svg", icon: "A", sourceLink: "aaaaa", color: "#14CCDC", selected: "false" })
// allSourcesList.push({ name: "CITYAM", img: "https://batonsystems.com/wp-content/uploads/2022/01/City-AM-Optimised-Logo.png", icon: "A", sourceLink: "aaaaa", color: "#009DDC", selected: "false" })
// allSourcesList.push({ name: "BANGKOKPOST", img: "https://logos-download.com/wp-content/uploads/2016/05/Bangkok_Post_logo_blue_wordmark.png", icon: "A", sourceLink: "aaaaa", color: "#213C70", selected: "false" })
// allSourcesList.push({ name: "APPLEINSIDER", img: "https://photos5.appleinsider.com/v10/ai-logo.png", icon: "A", sourceLink: "aaaaa", color: "#161C28", selected: "false" })
// allSourcesList.push({ name: "", img: "", icon: "A", sourceLink: "aaaaa", color: "#00417B", selected: "false" })
// allSourcesList.push({ name: "", img: "", icon: "A", sourceLink: "aaaaa", color: "#00417B", selected: "false" })
// allSourcesList.push({ name: "", img: "", icon: "A", sourceLink: "aaaaa", color: "#00417B", selected: "false" })
// allSourcesList.push({ name: "", img: "", icon: "A", sourceLink: "aaaaa", color: "#00417B", selected: "false" })
// allSourcesList.push({ name: "", img: "", icon: "A", sourceLink: "aaaaa", color: "#00417B", selected: "false" })
// allSourcesList.push({ name: "", img: "", icon: "A", sourceLink: "aaaaa", color: "#00417B", selected: "false" })
// allSourcesList.push({ name: "", img: "", icon: "A", sourceLink: "aaaaa", color: "#00417B", selected: "false" })
// allSourcesList.push({ name: "", img: "", icon: "A", sourceLink: "aaaaa", color: "#00417B", selected: "false" })
// allSourcesList.push({ name: "", img: "", icon: "A", sourceLink: "aaaaa", color: "#00417B", selected: "false" })
// allSourcesList.push({ name: "", img: "", icon: "A", sourceLink: "aaaaa", color: "#00417B", selected: "false" })
// allSourcesList.push({ name: "", img: "", icon: "A", sourceLink: "aaaaa", color: "#00417B", selected: "false" })
// allSourcesList.push({ name: "", img: "", icon: "A", sourceLink: "aaaaa", color: "#00417B", selected: "false" })
// allSourcesList.push({ name: "", img: "", icon: "A", sourceLink: "aaaaa", color: "#00417B", selected: "false" })