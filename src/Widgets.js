import React, { useState, useEffect, useRef, useContext } from 'react';
import {AppContext} from "./App"
import "./Widgets.css";
import SideMenuList from "./components/SideMenuList"
import SourceItem from "./components/SourceItem"
import { ReactComponent as LeftIcon } from "./resources/arrow.svg"

function Widgets(props) {
  	const context = useContext(AppContext)

	function handleLightsOff(event) {
		document.getElementById('widgets').style.backgroundColor = '#282828'  ; 
		document.getElementById('widgets').style.color = '#AAAAAA'  ; 
	  }
	return (

		<div id="widgets" className="widgets" >
			<SideMenuList sideMenus={context.rightSideMenus}/>

		</div>
	);
}

export default Widgets;
