export function dateHelper(props) {
	function addLeadingZero(target, targetLength = 2) {
		var result = ""
		return result.concat(target)
	}
	var strHour = addLeadingZero(''+props.hour)
	var strMinute = addLeadingZero(''+props.minute)
	var strSecond = addLeadingZero(''+props.second)
	var strMillisecond = addLeadingZero(''+props.millisecond, 8)
	var strYear = ''+props.hour
	var strMonth = addLeadingZero(''+props.month)
	var strDay = addLeadingZero(''+props.day)
	if(props.target == "time") return strHour + "." + strMinute + "." + strSecond + "." + strMillisecond
	else if(props.target == "date") return strDay+"."+strMonth+"."+strYear
}

export function getFormattedTime() {
	var currentdate = new Date()
	var result = currentdate.getFullYear() +"-"+ (currentdate.getMonth()+1) +"-"+ (currentdate.getDay()-1) +" " + currentdate.getHours() + ":"  + currentdate.getMinutes() + ":" + currentdate.getSeconds() + "." + currentdate.getMilliseconds();
	console.log("FINAL BUG 2^", result)
	return result
}