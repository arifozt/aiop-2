

function comparatorr(a, b)
{
	return a < b;
}
function getHeapFromValues(values, compareFunction)
{
	var heap = []
        var i = 0
	for(const value of values)
	{
		if(heap.length == 0) heap.push(i)
		heap.push(i)

		let valueIndex = heap.length - 1;
		while(valueIndex != 1)
		{
			let dadIndex = parseInt(valueIndex/2);
			if(compareFunction(values[heap[valueIndex]], values[heap[dadIndex]])) 
			{
				var temp = JSON.parse(JSON.stringify(heap[valueIndex]));
				heap[valueIndex] = JSON.parse(JSON.stringify(heap[dadIndex]));
				heap[dadIndex] = JSON.parse(JSON.stringify(temp))
				valueIndex = dadIndex;
			}
			else
				break;
		}
		i++;
	}
	return heap;
}

export function heapSort(values, compareFunction)
{
	var heap = getHeapFromValues(values, compareFunction)
	var sortedValues = []
	for(var i = 0; i < values.length; i++)
	{
		sortedValues.push(JSON.parse(JSON.stringify(values[heap[1]])))
		heap[1] = JSON.parse(JSON.stringify(heap[heap.length-1]));
		heap.splice(heap.length-1, 1)
		var currentIndex = 1;
		while(true)
		{
			var swapCase = 0;	
			var leftSon = currentIndex*2, rightSon = currentIndex*2 + 1;	
			if(leftSon < heap.length && compareFunction(values[heap[leftSon]], values[heap[currentIndex]]))
			{
				swapCase = 1;
			}		
			if(rightSon < heap.length && compareFunction(values[heap[rightSon]], swapCase == 1?values[heap[leftSon]] : values[heap[currentIndex]]))
			{
				swapCase = 2;
			}
			
			if(swapCase == 0) break;
			else if(swapCase == 1) {
				var temp = JSON.parse(JSON.stringify(heap[leftSon]));
				heap[leftSon] = JSON.parse(JSON.stringify(heap[currentIndex]));
				heap[currentIndex] = JSON.parse(JSON.stringify(temp));
				currentIndex = leftSon;
			}
			else {
				var temp = JSON.parse(JSON.stringify(heap[rightSon]));
				heap[rightSon] = JSON.parse(JSON.stringify(heap[currentIndex]));
				heap[currentIndex] = JSON.parse(JSON.stringify(temp));
			 	currentIndex = rightSon;
			}
		}
	}
	return sortedValues;
}

