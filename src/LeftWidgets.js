import { withTheme } from "@material-ui/core";
import {AppContext} from "./App"
import React, { useState, useContext } from "react";
import MainMenu from "./components/MainMenu"
import SideMenuList from "./components/SideMenuList"
import "./LeftWidgets.css";
import { ReactComponent as SearchIcon } from './resources/searchIcon.svg';

function LeftWidgets(props) {
  const context = useContext(AppContext)
	function show_hide(event) {
		var div = document.getElementById('newpost');
		if (div.style.display !== 'none') {
			div.style.display = 'none';
		}
		else {
			div.style.display = 'block';
		}
	}
  const [tags, setTags] = useState(props.tags)
  function handleTagSearchChanged(event) {
    let searchTerm = event.target.value.toLowerCase()
    let newTags = props.tags.filter(tag => { return (searchTerm.length != 0) ? (tag.name.toLowerCase().search(searchTerm) != -1) : true });
    setTags(newTags)
  }
  return (
    <div id="test1" className="test1">

      <MainMenu  handleTagSearchChanged={handleTagSearchChanged} tags={props.tags} toggleTagByName={props.toggleTagByName} handleTagChange={props.handleTagChange} sourcesVisible={props.sourcesVisible} setSourcesVisible={props.setSourcesVisible} tagsVisible={props.tagsVisible} setTagsVisible={props.setTagsVisible} inFolders={props.inFolders} setInFolders={props.setInFolders} setFoldersToFeed={props.setFoldersToFeed} setHiddenAsPosts={props.setHiddenAsPosts} setDislikedAsPosts={props.setDislikedAsPosts} searchInTitle={props.searchInTitle} setSearchInTitle={props.setSearchInTitle} searchInBody={props.searchInBody} setSearchInBody={props.setSearchInBody} searchInTags={props.searchInTags} setSearchInTags={props.setSearchInTags} searchInAuthors={props.searchInAuthors} setSearchInAuthors={props.setSearchInAuthors} setLikedAsPosts={props.setLikedAsPosts} setAllAsPosts={props.setAllAsPosts} setHistoryAsPosts={props.setHistoryAsPosts} setSavedAsPosts={props.setSavedAsPosts} findSearchedPosts={props.findSearchedPosts} />
      <SideMenuList sideMenus={context.leftSideMenus}/>
    </div>
  );
}

export default LeftWidgets;
