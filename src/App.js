import React, { useState, useEffect, useRef } from 'react';
import LeftWidgets from "./LeftWidgets";
import Header from "./Header";
import Widgets from "./Widgets";
import Main from "./Main";
import SourceList from "./components/SourceList"
import Feed from "./components/Feed";
import "./App.scss";
import { ReactComponent as SearchIcon } from './resources/searchIcon.svg';
import MessagesDropDownButton from './components/MessagesDropDownButton'
import { BrowserRouter as Router } from "react-router-dom";
import { heapSort } from "./utils/Sorter"
import { storage } from 'firebase';
import * as $ from 'jquery';
import { data } from 'jquery';
import { spotify_api, spotify_api_search } from "./api/api"
import { youtube_api_search } from "./api/api"
import { twitch_api, twitch_api_search } from "./api/api"
import { twitter_api_search, tiktok_api_search, wikipedia_api_search } from "./api/api"
import { getTwits } from "./api/twitter"
import { search_api } from "./api/api"
import { bbc_api } from "./api/api"
import { setChatsListener, getMessages, sendMessage } from "./api/api"
import { setUserData, getUserData } from "./api/api"
import { trending_api } from "./api/api"
import { guardian_api, guardian_api_search } from "./api/api"
import { reddit_api, reddit_api_search } from './api/api'
import { firebase_api_sources, firebase_api } from './api/api'
import { firebaseApi } from './api/firebaseApi'
import { sourceImageMap } from './constants/data.js'
import { sourceBorderColorMap } from './constants/data.js'
import { sourcesList } from './constants/data.js'
import logo from './scoopt4.png';

export const AppContext = React.createContext({})
var postsLoading = []

var currentDate = "2022-04-04"
function compareDates(date1, date2) {
	if (date1 == undefined) return true
	if (date1.length < 4) return true
	return date1.localeCompare(date2) != 0
}
function getSearchApiByName(name, username, setQueuedPosts, searchTerm, SPOTIFY_ACCESS_TOKEN) {
	postsLoading.push(name)
	if (name == "TWITCH") twitch_api_search({ setPosts: setQueuedPosts, searchTerm: searchTerm })
	else if (name == "YOUTUBE") search_api({ searchTerm: searchTerm, source: name.toLowerCase(), username: username, setPosts: setQueuedPosts })
	else if (name == "TIKTOK") search_api({ searchTerm: searchTerm, source: name.toLowerCase(), username: username, setPosts: setQueuedPosts })
	else if (name == "GUARDIAN") guardian_api_search({ setPosts: setQueuedPosts, searchTerm: searchTerm })
	else if (name == "WIKIPEDIA") search_api({ searchTerm: searchTerm, source: name.toLowerCase(), username: username, setPosts: setQueuedPosts })
	else if (name == "REDDIT") reddit_api_search({ setPosts: setQueuedPosts, searchTerm: searchTerm })
	else if (name == "SPOTIFY") spotify_api_search({ SPOTIFY_ACCESS_TOKEN: SPOTIFY_ACCESS_TOKEN, setPosts: setQueuedPosts, searchTerm: searchTerm })
	else if (name == "TWITTER") search_api({ searchTerm: searchTerm, source: name.toLowerCase(), username: username, setPosts: setQueuedPosts })
	else postsLoading.splice(postsLoading.findIndex(post => post.includes(name)), 1)
}
function getApiByName(name, username, setQueuedPosts, SPOTIFY_ACCESS_TOKEN) {
	function emptyResponseSet() {
		postsLoading.splice(postsLoading.findIndex(post => post.localeCompare(name) == 0), 1)
	}
	console.log("Search queue posts ", postsLoading)
	console.log(name)
	postsLoading.push(name)
	if (name == "TWITCH") twitch_api({ setPosts: setQueuedPosts })
	else if (name == "YOUTUBE") trending_api({ source: name.toLowerCase(), username: username, setPosts: setQueuedPosts })
	else if (name == "BBC") bbc_api({ setPosts: setQueuedPosts })
	else if (name == "WIKIPEDIA") trending_api({ source: name.toLowerCase(), username: username, setPosts: setQueuedPosts })
	else if (name == "GUARDIAN") guardian_api({ setPosts: setQueuedPosts })
	else if (name == "REDDIT") reddit_api({ setPosts: setQueuedPosts })
	else if (name == "SPOTIFY") spotify_api({ SPOTIFY_ACCESS_TOKEN: SPOTIFY_ACCESS_TOKEN, setPosts: setQueuedPosts })
	else if (name == "TIKTOK") trending_api({ source: name.toLowerCase(), username: username, setPosts: setQueuedPosts })
	else if (name == "TWITTER") trending_api({ source: name.toLowerCase(), username: username, setPosts: setQueuedPosts })
	else firebase_api({ sourceName: name, setPosts: setQueuedPosts, emptyResponseSet: emptyResponseSet })
}
function sourcify(sourceName, img, color) {
	return ({ name: sourceName == undefined ? "NEW SOURCE" : sourceName.toUpperCase(), img: img == undefined ? "https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/The_Guardian_2018.svg/2560px-The_Guardian_2018.svg.png" : img, icon: "A", sourceLink: "aaaaa", color: color == undefined ? "#6093DE" : color, selected: "false" })

}
function savePostsToNuke() {

}
var allPosts = []

var firebaseSourcesAdded = false
var allSourcesList = JSON.parse(JSON.stringify(Array.from(new Set(sourcesList))))
var tempList = []
for (var source of allSourcesList) {
	if (tempList.find(tempSource => { return source.name == tempSource.name }) == undefined)
		tempList.push(source)
}
allSourcesList = tempList
var allTabs = [{ name: 'Tab 1', selected: true, allSources: JSON.parse(JSON.stringify(allSourcesList)), searchTags: [] }]
var allFolders = [
	{
		title: "Favourites",
		subfolders: [],
		posts: [],
		path: []
	}]
var tabNo = allTabs.length + 1;
var activeTabIndex = 0;


var loadedUserData = false

var newFolderTitle = "New Folder"

var lastAddedSource = ""

var searchTerm = "";

var allVarSideMenus = []

var allSearchTags = []

var messagesNo = 0
function getMessagesNo() {
	return messagesNo
}

function setMessagesNo(newValue) {
	messagesNo = newValue
}

var globalAllSources = JSON.parse(JSON.stringify(allTabs[activeTabIndex].allSources))

var guestId = Math.floor(Math.random() * 10000000).toString()

var localUsername = localStorage.getItem('username') == null ? "" : localStorage.getItem('username')
var localLogged = localStorage.getItem('logged') == null ? false : localStorage.getItem('logged')
var localEmail = localStorage.getItem('email') == null ? "" : localStorage.getItem('email')

var globalQueuedMessages = []

function setGlobalQueuedMessages(newValue) {
	globalQueuedMessages = newValue
}

var globalMessages = {}
function setGlobalMessages(newValue) {
	globalMessages = newValue
}
function App(props) {
	if (activeTabIndex > allTabs.length) activeTabIndex = 0
	const allTagsSet = new Set()
	for (const post of allPosts) {
		for (const tag of post.tags) allTagsSet.add({ name: tag, selected: false })
	}
	let allTags = Array.from(allTagsSet)
	allTags.push({ name: "monkey", selected: false })
	{/*   we need  a useEffect which grabs the source name and runs the getApiByName function */ }
	console.log("ALL TABS ARE", allTabs, activeTabIndex)

	const [allContacts, setAllContacts] = useState([])
	const [messages, setMessages] = useState({})
	const [queuedMessages, setQueuedMessages] = useState([])

	const [chats, setChats] = useState([])

	const [username, setUsername] = useState(localUsername)
	const [logged, setLogged] = useState(localLogged)
	const [email, setEmail] = useState(localEmail)
	const [messagesLoading, setMessagesLoading] = useState(false)
	const [messagesSetLoading, setMessagesSetLoading] = useState(false)

	const [tabs, setTabs] = useState(allTabs)
	console.log("BUG TRACK ", allTabs, activeTabIndex, loadedUserData)
	const [allSources, setAllSources] = useState(allTabs[Math.max(allTabs.length - 1, activeTabIndex)].allSources == undefined ? JSON.parse(JSON.stringify(allSourcesList)) : (allTabs[Math.max(allTabs.length - 1, activeTabIndex)].allSources))
	const [trigger, setTrigger] = useState(-1)
	console.log("USERNAME IS ", username)
	const [searchTags, setSearchTags] = useState(allTabs[Math.max(allTabs.length - 1, activeTabIndex)].searchTags)

	const [posts, setPosts] = useState([]);
	const [filteredPosts, setFilteredPosts] = useState([])
	const [queuedPosts, setQueuedPosts] = useState([])
	const [likedPosts, setLikedPosts] = useState([]);
	const [savedPosts, setSavedPosts] = useState([]);
	const [history, setHistory] = useState([]);
	const [selectedTags, setSelectedTags] = useState([])

	const [summaryVisible, setSummaryVisible] = useState(false);
	const [summaryBody, setSummaryBody] = useState("");

	const [videosVisible, setVideosVisible] = useState(false)
	const [videosBody, setVideosBody] = useState("")

	const [sourcesVisible, setSourcesVisible] = useState(true)

	const [tagsVisible, setTagsVisible] = useState(false)


	const [folders, setFolders] = useState(allFolders)
	const [inFolders, setInFolders] = useState(false)
	const [folderPath, setFolderPath] = useState([])

	const [searchInTitle, setSearchInTitle] = useState(true)
	const [searchInBody, setSearchInBody] = useState(true)
	const [searchInTags, setSearchInTags] = useState(true)
	const [searchInAuthors, setSearchInAuthors] = useState(true)
	const [scrollValue, setScrollValue] = useState(0)

	const [unseenMessages, setUnseenMessages] = useState({})


	const [searchButtonClicked, setSearchButtonClicked] = useState(false);

	const [SPOTIFY_ACCESS_TOKEN, setSPOTIFY_ACCESS_TOKEN] = useState("")

	const [sourcesSearchTerm, setSourcesSearchTerm] = useState("")

	const [varSideMenus, setVarSideMenus] = useState(allVarSideMenus)
	function getMessages() {
		return globalMessages
	}

	var sourcesSideMenu = {
		title: "Sources", body: <div>{(!searchButtonClicked) ?
			<button className='btn-search' onClick={handleSearchClicked}><SearchIcon className='searchIcon' /> </button>
			:
			<label>
				<br></br>
				<input type="text" placeholder="Type to Search..." className="input-search" id="source-search" onChange={handleSearchChange} />
			</label>}
			<br></br>
			<SourceList scrollValue={scrollValue} setScrollValue={setScrollValue} sources={sourcesSearchTerm.length == 0 ? allSources : allSources.filter(source => source.name.toLowerCase().includes(sourcesSearchTerm.toLowerCase()))} setSources={setAllSources} updatePosts={recalculatePosts} onClick={handleSourceClicked} key={JSON.stringify(allSources)} />
		</div>, setVisible: (title) => { setSourcesVisible(false); removeSummary(title); }, visible: sourcesVisible
	}
	var leftSideMenus = varSideMenus.length != 0 ? [sourcesSideMenu] : []
	var rightSideMenus = varSideMenus.length == 0 ? [sourcesSideMenu] : allVarSideMenus

	function getPosts() {
		return posts
	}
	useEffect(() => {
		var newUnseenMessages = {}
		for (const receiver of Object.keys(messages)) {
			newUnseenMessages[receiver] = messages[receiver].filter(message => { return !message.seen && !message.sent }).length
			console.log("UNSEEN CHECK", newUnseenMessages[receiver])
		}
		setUnseenMessages(newUnseenMessages)
	}, [messages])
	useEffect(() => { localStorage.setItem('logged', logged); localStorage.setItem('username', username); localStorage.setItem('email', email); }, [logged, username, email])
	useEffect(() => { var maxHeight = 1000000; document.getElementsByClassName("sideMenuList")[1].scrollTo({ top: maxHeight }) }, [varSideMenus])
	useEffect(() => {
		if (logged == false) {
			loadedUserData = false
			setUsername("guest" + guestId)
			activeTabIndex = 0
			allTabs = [{ name: 'Tab 1', selected: true, allSources: JSON.parse(JSON.stringify(allSourcesList)), searchTags: [] }]
			setTabs([{ name: 'Tab 1', selected: true, allSources: JSON.parse(JSON.stringify(allSourcesList)), searchTags: [] }])
			setNewTab()

		}
		else {
			if (loadedUserData == false) {
				function setActiveTabIndex(newValue) {
					activeTabIndex = newValue
				}
				function setAllTabs(newValue) {
					if (activeTabIndex > newValue.length) activeTabIndex = 0
					allTabs = newValue
				}
				function setAllFolders(newValue) {
					allFolders = newValue
				}
				function setLoadedUserData(newValue) {
					loadedUserData = newValue
				}

				//getMessages({username: username, messages: messages, setMessages: setMessages, messagesLoading: messagesLoading, setMessagesLoading: setMessagesLoading, setMessagesNo: setMessagesNo})
				console.log("SET CHATS LISTENER @")
				setChatsListener({ username: username, globalMessages: globalMessages, setGlobalMessages: setGlobalMessages, getMessages: getMessages, setMessages: setMessages, messagesLoading: messagesLoading, setMessagesLoading: setMessagesLoading, getMessagesNo: getMessagesNo, setMessagesNo: setMessagesNo })
				getUserData({ setAllContacts: setAllContacts, username: username, activeTabIndex: activeTabIndex, setActiveTabIndex: setActiveTabIndex, allTabs: allTabs, setAllTabs: setAllTabs, setTabs: setTabs, allFolders: allFolders, setAllFolders: setAllFolders, setFolders: setFolders, setNewTab: setNewTab, setLoadedUserData: setLoadedUserData })

			}
		}
	}, [logged])
	useEffect(() => {
		if (logged == false) return
		if (loadedUserData == false) {
			return
		}
		console.log("CHATS BUG 2", allContacts, globalAllSources)
		setUserData({ allTabs: allTabs, username: username, email: email, allContacts: allContacts, globalAllSources: globalAllSources, searchTags: searchTags, activeTabIndex: activeTabIndex, allFolders: allFolders })

	}, [allSources, searchTags, tabs, activeTabIndex, allContacts, folders])
	useEffect(() => { if (tabs[activeTabIndex].selected == false) setTabsColor() }, [tabs])
	useEffect(() => {
		var actualUrl = window.location.href
		if (actualUrl.includes("access_token")) {
			var tokenStartIndex = -1, tokenEndIndex = actualUrl.length
			for (var i = 12; i < tokenEndIndex; i++) {
				if (actualUrl.substring(i - 13, i).localeCompare('access_token=') == 0) {
					tokenStartIndex = i;
					break;
				}
			}
			if (tokenStartIndex == -1) {
				console.log("NO START FOR ACCESS TOKEN")
				return
			}
			setSPOTIFY_ACCESS_TOKEN(actualUrl.substring(tokenStartIndex, tokenEndIndex))
			console.log('INDEXES ARE ', tokenStartIndex, tokenEndIndex)
		}

	}, [window.location.href])
	useEffect(() => {
		if (videosVisible)
			setTagsVisible(false)
	}, [videosVisible])
	useEffect(() => {
		console.log(queuedPosts)
		console.log("queue effect trigger ", queuedPosts, typeof (queuedPosts), queuedPosts.length, activeTabIndex, postsLoading)
		console.log(typeof (queuedPosts))
		if (queuedPosts.length != 0) {
			console.log("queue effect passed checks")
			if (postsLoading.find(post => post.toLowerCase().localeCompare(queuedPosts[0].for_who.toLowerCase()) == 0) != undefined) {
				var newPosts = JSON.parse(JSON.stringify(posts))
				newPosts = (JSON.parse(JSON.stringify(queuedPosts))).concat(newPosts)
				console.log("NEW POSTS ARE ", newPosts)
				allPosts = JSON.parse(JSON.stringify(newPosts))
				setPosts(newPosts)
				postsLoading.splice(postsLoading.findIndex(post => post.includes(queuedPosts[0].for_who)), 1)
			}
			setQueuedPosts([])
		}
	}, [queuedPosts])
	useEffect(() => {
		console.log("trigger is", trigger)
		if (trigger == -1) return
		if (allSources[trigger].selected == true) {
			getApiByName(allSources[trigger].name, username, setQueuedPosts, SPOTIFY_ACCESS_TOKEN)
			savePostsToNuke()
			console.log("effect hook ended", allSources[trigger].name)
			setTrigger(-1)
		}

	}, [trigger])

	useEffect(() => {
		if (allSources.filter(source => source.selected == true).length == 0) return
		var changedIndex = allSources.findIndex(source => lastAddedSource.includes(source.name))
		if (changedIndex == -1) {
			recalculatePosts()
			return
		}
		if (allSources[changedIndex].selected == false) return
		console.log("changed to in set trigger effect", changedIndex)
		setTrigger(changedIndex)
	}, [allSources])

	function sortByName(a, b) {
		return a.name < b.name;
	}
	function setLastAddedSource(name) {
		lastAddedSource = name
	}
	function addSummary(title, body, visible, setPostSummaryVisible) {
		function handleSummaryVisibilityChanged(title) {
			setPostSummaryVisible(false)
			removeSummary(title)
		}
		var newSideMenus = JSON.parse(JSON.stringify(varSideMenus))
		newSideMenus.push({ title: title, body: body, visible: visible, setVisible: handleSummaryVisibilityChanged })
		allVarSideMenus.push({ title: title, body: body, visible: visible, setVisible: handleSummaryVisibilityChanged })
		console.log("SUMMARY FUNCTION", newSideMenus[newSideMenus.length - 1].setVisible, typeof (newSideMenus[newSideMenus.length - 1].setVisible))
		setVarSideMenus(newSideMenus)
	}
	function removeSummary(title) {
		var newSideMenus = JSON.parse(JSON.stringify(allVarSideMenus))
		console.log("SIDE MENUS ARE ", varSideMenus)
		newSideMenus.forEach(menu => console.log("SIDE MENU TITLE ", menu.title))
		//newSideMenus.splice(newSideMenus.findIndex(sideMenu => sideMenu.title.localeCompare(title) == 0), 1)
		console.log("FOUND INDEX FOR", title, newSideMenus.findIndex(sideMenu => { return sideMenu.title.localeCompare(title) == 0 }))
		newSideMenus.splice(newSideMenus.findIndex(sideMenu => sideMenu.title.localeCompare(title) == 0), 1)
		allVarSideMenus.splice(allVarSideMenus.findIndex(sideMenu => sideMenu.title.localeCompare(title) == 0), 1)
		setVarSideMenus(newSideMenus)
	}
	function handleSourceClicked(event) {
		if (event.target.innerText == undefined || event.target.innerText.length < 3) return;
		let pickedSource = event.target.innerText;
		console.log("BUG SOURCEE", pickedSource)
		const pickedElement = globalAllSources.find((element) => { return pickedSource.includes(element.name); });
		console.log("BUG SOURCEE", pickedElement, allSourcesList, pickedSource)
		if (pickedElement.icon.toLowerCase() != pickedElement.icon.toUpperCase()) {
			let newSources = JSON.parse(JSON.stringify(globalAllSources))
			const newSourcesIndex = newSources.findIndex((element) => { return pickedSource.includes(element.name); })
			newSources[newSourcesIndex].icon = "-";
			newSources[newSourcesIndex].selected = true
			globalAllSources[newSourcesIndex].icon = "-";
			globalAllSources[newSourcesIndex].selected = true
			setAllSources(newSources);
		}
		else {
			let newSources = JSON.parse(JSON.stringify(globalAllSources))
			const newSourcesIndex = newSources.findIndex((element) => { return pickedSource.includes(element.name); })
			var postsLoadingIndex = postsLoading.findIndex(post => pickedSource.includes(post))
			console.log("BUG SOURCEE", postsLoading)
			if (postsLoadingIndex != -1) postsLoading.splice(postsLoadingIndex, 1)
			newSources[newSourcesIndex].icon = "A";
			newSources[newSourcesIndex].selected = false
			globalAllSources[newSourcesIndex].icon = "A";
			globalAllSources[newSourcesIndex].selected = false
			setAllSources(newSources);
		}
		setLastAddedSource(pickedSource)
		updatePostsWithSource(pickedSource)
		recalculatePosts()
	}
	function handleSearchClicked(event) {
		setSearchButtonClicked(true)
	}
	function makeNewFolder() {
		var newFolders = JSON.parse(JSON.stringify(allFolders))
		newFolders.push({
			title: newFolderTitle,
			subfolders: [],
			posts: [],
			path: JSON.parse(JSON.stringify(folderPath))
		})
		allFolders.push({
			title: newFolderTitle,
			subfolders: [],
			posts: [],
			path: JSON.parse(JSON.stringify(folderPath))
		})
		console.log("FOLDERS ARE ", allFolders, "PATH TO FIRST IS", allFolders[0].path, folderPath, JSON.stringify(folderPath), JSON.stringify(folderPath))
		newFolderTitle += " (1) "
		setFolders(newFolders)
	}
	function addTabButtonPressed(event) {
		let newTabs = JSON.parse(JSON.stringify(tabs))
		saveLastTab()
		console.log("SOURCES LIST FOR NEW TAAB", allSourcesList)
		allTabs.push({ name: 'Tab ' + tabNo, selected: false, allSources: JSON.parse(JSON.stringify(allSourcesList)), searchTags: [] })
		newTabs.push({ name: 'Tab ' + tabNo++, selected: false, allSources: JSON.parse(JSON.stringify(allSourcesList)), searchTags: [] })
		setTabs(newTabs)

		setTabActive({ target: { innerText: allTabs[allTabs.length - 1].name } })
	}
	function saveLastTab() {
		var newTabs = JSON.parse(JSON.stringify(allTabs))
		allTabs[activeTabIndex].allSources = JSON.parse(JSON.stringify(allSources))
		allTabs[activeTabIndex].searchTags = JSON.parse(JSON.stringify(searchTags))
		allTabs[activeTabIndex].selected = false

		newTabs[activeTabIndex].allSources = JSON.parse(JSON.stringify(allSources))
		newTabs[activeTabIndex].searchTags = JSON.parse(JSON.stringify(searchTags))
		newTabs[activeTabIndex].selected = false

		setTabs(newTabs)
	}
	function setNewTab() {
		postsLoading = []
		globalAllSources = JSON.parse(JSON.stringify(allTabs[activeTabIndex].allSources))
		setAllSources(allTabs[activeTabIndex].allSources)
		setSearchTags(allTabs[activeTabIndex].searchTags)
		for (source of allTabs[activeTabIndex].allSources) {
			if (source.selected == true) getApiByName(source.name, username, setQueuedPosts, SPOTIFY_ACCESS_TOKEN)
		}
		recalculatePosts()

	}
	function changeTabName(currentName, newName) {
		saveLastTab()
		var newTabs = JSON.parse(JSON.stringify(allTabs))
		for (var i = 0; i < allTabs.length; i++) {
			if (allTabs[i].name == currentName) {
				allTabs[i].name = newName
				newTabs[i].name = newName
				break
			}
		}
		setTabs(newTabs)
		setNewTab()
	}
	function folderClickedInPath(folderName) {
		var folderIndex = folders.findIndex(folder => folderName.replaceAll(" ", "").localeCompare(folder.title.replaceAll(" ", "")) == 0)
		setPosts(folders[folderIndex].posts)
		var newFolderPath = JSON.parse(JSON.stringify(folderPath))
		var folderIndexInPath = newFolderPath.findIndex(folder => folder.replaceAll(" ", "").localeCompare(folderName.replaceAll(" ", "")) == 0)
		newFolderPath.splice(folderIndexInPath + 1, newFolderPath.length - folderIndexInPath)
		setFolderPath(newFolderPath)
	}
	function addPostToFolder(folderName, newPost) {
		var folderIndex = folders.findIndex(folder => folderName.replaceAll(" ", "").includes(folder.title.replaceAll(" ", "")))
		console.log(folderIndex, folderName, folders)
		var newFolders = JSON.parse(JSON.stringify(allFolders))
		newFolders[folderIndex].posts.push(newPost)
		allFolders[folderIndex].posts.push(newPost)
		setFolders(newFolders)
	}
	function changeFolderName(currentName, newName) {
		var newFolders = JSON.parse(JSON.stringify(allFolders))
		for (var i = 0; i < folders.length; i++) {
			if (folders[i].title == currentName) {
				allFolders[i].title = newName
				newFolders[i].title = newName
			}
			newFolders[i].path = JSON.parse(JSON.stringify(folders[i].path).replaceAll(currentName, newName))
			allFolders[i].path = JSON.parse(JSON.stringify(folders[i].path).replaceAll(currentName, newName))
		}
		setFolders(newFolders)
	}
	function deleteFolderByName(name) {
		var newFolders = JSON.parse(JSON.stringify(allFolders))
		for (var i = 0; i < folders.length; i++) {
			if (folders[i].title == name) {
				allFolders.splice(i, 1)
				newFolders.splice(i, 1)
				break
			}
		}
		setFolders(newFolders)
	}
	function setTabsColor() {
		var newTabs = JSON.parse(JSON.stringify(allTabs))
		for (var i = 0; i < allTabs.length; i++) {
			allTabs[i].selected = false
			newTabs[i].selected = false
			if (i == activeTabIndex) {
				allTabs[i].selected = true
				newTabs[i].selected = true
			}
		}
		setTabs(newTabs)
	}
	function setTabActive(event) {
		saveLastTab()
		let tabTitle = event.target.innerText;
		console.log("clicked on", tabTitle)
		for (var i = 0; i < allTabs.length; i++) {
			if (tabTitle == allTabs[i].name) {
				activeTabIndex = i;
			}
		}
		setTabsColor()
		console.log(tabTitle, activeTabIndex)
		setNewTab()
	}
	function closeTab(tabName) {
		var tabToDelete = -1
		for (var i = 0; i < allTabs.length; i++) {
			if (tabName == allTabs[i].name) {
				tabToDelete = i;
			}
		}
		var newTabs = JSON.parse(JSON.stringify(tabs))
		newTabs.splice(tabToDelete, 1)
		allTabs.splice(tabToDelete, 1)
		setTabs(newTabs)
		if (tabToDelete < activeTabIndex) {
			--activeTabIndex
			setTabsColor()
			setNewTab()
		}
		if (tabToDelete == activeTabIndex) {
			activeTabIndex = 0
			setTabsColor()
			setNewTab()
		}
	}
	function compareByTitle(postA, postB) {
		return ((postA.title == undefined) ? "" : postA.title).localeCompare(postB.title) == -1;
	}
	function compareByDate(postA, postB) {
		return ((postA.subreddit == undefined) ? "" : postA.subreddit).localeCompare(postB.subreddit) == -1;
	}
	function compareByLikes(postA, postB) {
		return postA.upvote < postB.upvote;
	}
	function getHiddenPost() {
		return ({
			upvote: 0,
			liked: "untouched",
			saved: false,
			hidden: true,
			for_who: "",
			image: null,
			title: "",
			summary: "",
			full_text: "",
			comments_count: 70,
			tags: "",
			user: "John Smith ",
			subreddit: "01-02-2022",
			url: ""
		})
	}
	function toggleTagByName(tagName) {
		console.log("BUG TAG", tagName)
		for (var i = 0; i < allTags.length; ++i) {
			if (tagName.includes(allTags[i].name)) {
				allTags[i].selected = !allTags[i].selected;
				updatePostsWithTag(allTags[i])
				break;
			}
		}
	}

	function findPostsWithTags(tags) {
		console.log("TAGS selected", tags)
		var newPosts = []
		allPosts.forEach(post => {
			var newTags = post.tags.filter(tag => {
				var inTags = false
				for (var i = 0; i < tags.length; i++)
					if (tags[i].name.localeCompare(tag) == 0) {
						inTags = true
						break
					}
				if (tags.length == 0) inTags = true
				return inTags
			})
			if (newTags.length != 0) newPosts.push(post)
			console.log("bug tags", newTags, post.tags)
		})
		console.log("BUG TAGS", newPosts, tags)
		setPosts(newPosts)
	}

	function updatePostsWithTag(newTag) {
		// to do: add tag as searchtag to searchtags
		var newSearchTags = JSON.parse(JSON.stringify(allSearchTags))
		if (newSearchTags.find(searchTag => (searchTag.name == newTag.name && searchTag.type == "tag")) == undefined) {
			newSearchTags.push({ name: newTag.name, type: "tag" })
			allSearchTags.push({ name: newTag.name, type: "tag" })
		}
		else {
			newSearchTags.splice(newSearchTags.findIndex(searchTag => (searchTag.name == newTag.name && searchTag.type == "tag")), 1)
			allSearchTags.splice(allSearchTags.findIndex(searchTag => (searchTag.name == newTag.name && searchTag.type == "tag")), 1)
		}
		setSearchTags(newSearchTags)
		findPostsWithTags(newSearchTags.filter(tag => tag.type == "tag"))
	}

	function updatePostsWithSource(newSourceName) {
		// to do: add tag as searchtag to searchtags
		var newSearchTags = JSON.parse(JSON.stringify(allSearchTags))
		if (newSearchTags.find(searchTag => (searchTag.name == newSourceName && searchTag.type == "source")) == undefined) {/*
			newSearchTags.push({name: newSourceName, type: "source"})
			allSearchTags.push({name: newSourceName, type: "source"})
		*/}
		else {/*
			newSearchTags.splice(newSearchTags.findIndex(searchTag => (searchTag.name == newSourceName && searchTag.type=="source")), 1)
			allSearchTags.splice(allSearchTags.findIndex(searchTag => (searchTag.name == newSourceName && searchTag.type=="source")), 1)
		*/}
		setSearchTags(newSearchTags)
	}

	function handleTagChange(event) {
		toggleTagByName(event.target.innerText)
	}

	function setFoldersToFeed() {
		setInFolders(true)
		setFolderPath([])
		setPosts([])
	}
	function recalculatePosts() {
		console.log("recalculating ", activeTabIndex, tabs[activeTabIndex])
		console.log("posts are ", allPosts)
		let newPosts = JSON.parse(JSON.stringify(posts));
		newPosts = newPosts.filter(post => {
			if (post.for_who != undefined) if (globalAllSources.find(source => { return source.name.toLowerCase().includes(post.for_who.toLowerCase()) && source.selected == true })) return post;
		})
		console.log("BUG SOURCEE posts are ", newPosts, allSources, globalAllSources)
		setPosts(newPosts)
	}
	function inSelectedSources(post) {
		return allSources.find(source => { return source.name == post.for_who && source.selected == true });
	}

	function setSearchResultsInFeed(searchTerm) {
		setPosts([])
		allSources.forEach((source) => {
			if (source.selected == true)
				getSearchApiByName(source.name, username, setQueuedPosts, searchTerm, SPOTIFY_ACCESS_TOKEN)
		})
	}
	function findSearchedPosts(event) {
		let searchTerm = event.target.value.toLowerCase()
		console.log("Searched for ", searchTerm, " in posts")
		setSearchResultsInFeed(searchTerm)
	}

	function setDislikedAsPosts() {
		var newPosts = allPosts.filter(post => { if (post.liked == "disliked") return post; })
		setPosts(newPosts)
	}

	function setHiddenAsPosts() {
		var newPosts = allPosts.filter(post => { if (post.hidden == true) return post; })
		for (var i = 0; i < newPosts.length; i++) newPosts[i].hidden = false
		setPosts(newPosts)
	}

	function setLikedAsPosts() {
		var newPosts = allPosts.filter(post => { if (post.liked == "liked") return post })
		setPosts(newPosts)
	}

	function setHistoryAsPosts() {
		setPosts(history)
	}


	function setSavedAsPosts() {
		setPosts(savedPosts)
	}

	function sortByTitleClicked(event, sortState) {
		let newPosts = JSON.parse(JSON.stringify(heapSort(JSON.parse(JSON.stringify(posts)), compareByTitle)))
		if (sortState != "untouched") newPosts.reverse()
		newPosts.push(getHiddenPost())
		setPosts(newPosts)
		console.log(posts)

	}
	function sortByLikesClicked(event, sortState) {
		let newPosts = JSON.parse(JSON.stringify(heapSort(JSON.parse(JSON.stringify(posts)), compareByLikes)))
		if (sortState != "untouched") newPosts.reverse()
		newPosts.push(getHiddenPost())
		setPosts(newPosts)
		console.log(posts)
	}
	function sortByDateClicked(event, sortState) {
		let newPosts = JSON.parse(JSON.stringify(heapSort(JSON.parse(JSON.stringify(posts)), compareByDate)))
		if (sortState != "untouched") newPosts.reverse()
		newPosts.push(getHiddenPost())
		setPosts(newPosts)
		console.log(posts)
	}
	function handleSearchChange(event) {
		console.log(event.target.value)
		let value = event.target.value.toLowerCase();
		setSourcesSearchTerm(value);
		console.log(event.target.value)
	}
	var fllg = "0"
	function handleLightsOff(event) {
		if (fllg == "1") {
			console.log("ITS BLAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACK")
			document.getElementById('app').style.backgroundColor = '#fcfcfc';
			document.getElementById('main').style.backgroundColor = '#F4f5fA';
			document.getElementById('feed').style.backgroundColor = 'white';
			document.getElementById('app').style.color = '#000000';
			document.getElementById('sesese').style.backgroundColor = '#AAAAAA';
			try {
				document.getElementById('aaas').style.backgroundColor = '#AAAAAA';
				document.getElementById('assa').style.backgroundColor = '#AAAAAA';
			} catch (e) {
				console.log(" ")
			}
			fllg = 0
		} else {
			fllg = 1
			console.log("ITS WHITTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTE")
			document.getElementById('app').style.backgroundColor = '#212121';
			document.getElementById('main').style.backgroundColor = '#181818';
			document.getElementById('feed').style.backgroundColor = '#282828';
			document.getElementById('app').style.color = '#AAAAAA';
			document.getElementById('sesese').style.backgroundColor = '#212121';

			try {
				document.getElementById('aaas').style.backgroundColor = '#282828';
				document.getElementById('assa').style.backgroundColor = '#282828';
			} catch (e) {
				console.log(" ")
			}
		}
	}
	function handleKeyPressed(event) {
		console.log(event.key)
		if (event.key == "Enter")
			findSearchedPosts(event)
	}


	function closeMenu() {
		var button = document.getElementById('closee'); // Assumes element with id='button'
		button.onclick = function () {
			var div = document.getElementById('mainmenuu');
			if (div.style.display !== 'none') {
				div.style.display = 'none';
			}
			else {
				div.style.display = 'block';
			}
		}
	}
	function handleHomeClicked(event) {
		setInFolders(false)
		recalculatePosts()
	}
	return (
		// BEM
		<div style={{height:"100vh", overflow:"hidden"}}>
<AppContext.Provider value={{ "email": email, "setEmail": setEmail, "tabs": tabs, "logged": logged, "setLogged": setLogged, "username": username, "setUsername": setUsername, "leftSideMenus": leftSideMenus, "rightSideMenus": rightSideMenus, "allSources": allSources, "allContacts": allContacts, "setAllContacts": setAllContacts, "messages": messages, "setMessages": setMessages, "queuedMessages": queuedMessages, "setQueuedMessages": setQueuedMessages, "globalQueuedMessages": globalQueuedMessages, "setGlobalQueuedMessages": setGlobalQueuedMessages, "getMessagesNo": getMessagesNo, "setMessagesNo": setMessagesNo, "getMessages": getMessages, "globalMessages": globalMessages, "setGlobalMessages": setGlobalMessages, "unseenMessages": unseenMessages, "setUnseenMessages": setUnseenMessages }}>
			<div style={{ display:"flex",borderBottom: "2px solid black", backgroundColor: "#373737", paddingBottom: "0.5vh" }}><div style={{ display:"flex", marginRight:"1vw", marginLeft:"0.3vw"}}><div style={{width:"1vw",display:"block"}}><button id="lights" className="button-28" style={{ backgroundColor: "white", width: "fit-content", height: "fit-content" }} onClick={handleLightsOff}></button><button style={{ width: "fit-content" }} className="button-28" id="closee" onClick={closeMenu} ></button></div><img src={logo} alt="Site logo" style={{ marginTop: "0.5vh"}} width="300" height="45" onClick={handleHomeClicked}></img></div>
				<input className="centralSearch" id="sesese" placeholder="Type to Search..." icon onKeyPress={handleKeyPressed}></input><SearchIcon style={{ marginTop:"1.2vh" ,width: "2vh", height: "2vh", paddingLeft: "1vh"}} />			<MessagesDropDownButton/>
			</div>
			<div id="app" className="app">
					<LeftWidgets setLastAddedSource={setLastAddedSource} handleSearchChange={handleSearchChange} updatePosts={recalculatePosts} videosVisible={videosVisible} sourcesVisible={sourcesVisible} setSourcesVisible={setSourcesVisible} setTagsVisible={setTagsVisible} tagsVisible={tagsVisible} inFolders={inFolders} setInFolders={setInFolders} setFoldersToFeed={setFoldersToFeed} setHiddenAsPosts={setHiddenAsPosts} setDislikedAsPosts={setDislikedAsPosts} searchInTitle={searchInTitle} setSearchInTitle={setSearchInTitle} searchInBody={searchInBody} setSearchInBody={setSearchInBody} searchInTags={searchInTags} setSearchInTags={setSearchInTags} searchInAuthors={searchInAuthors} setSearchInAuthors={setSearchInAuthors} toggleTagByName={toggleTagByName} key={allTags} tags={allTags} handleTagChange={handleTagChange} setAllAsPosts={recalculatePosts} setHistoryAsPosts={setHistoryAsPosts} setLikedAsPosts={setLikedAsPosts} setSavedAsPosts={setSavedAsPosts} />
					<Router>
						<Main>
							<Header searchTags={searchTags} changeTabName={changeTabName} findSearchedPosts={findSearchedPosts} setTabActive={setTabActive} toggleTagByName={toggleTagByName} closeTab={closeTab} addTabButtonPressed={addTabButtonPressed} tabs={tabs} setTabs={setTabs} sortByLikesClicked={sortByLikesClicked} sortByDateClicked={sortByDateClicked} sortByTitleClicked={sortByTitleClicked} />
							<Feed username={username} removeSummary={removeSummary} addSummary={addSummary} setVideosVisible={setVideosVisible} setVideosBody={setVideosBody} folderClickedInPath={folderClickedInPath} folderPath={folderPath} addPostToFolder={addPostToFolder} setFolderPath={setFolderPath} setFolders={setFolders} deleteFolderByName={deleteFolderByName} changeFolderName={changeFolderName} folders={folders} makeNewFolder={makeNewFolder} inFolders={inFolders} key={allSources} activeTabIndex={activeTabIndex} toggleTagByName={toggleTagByName} allPosts={allPosts} posts={posts} setPosts={setPosts} history={history} likedPosts={likedPosts} savedPosts={savedPosts} setSummaryVisible={setSummaryVisible} setSummaryBody={setSummaryBody} />
						</Main>
					</Router>
					<Widgets />
			</div>
				</AppContext.Provider>
		</div>
	);
}

export default App;

