import React from 'react'
import './Main.css'

function Main(props) {
    return (
        <div id="main" className="main">
            {props.children}
        </div>
    )
}

export default Main
