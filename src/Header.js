import { getThemeProps } from "@material-ui/styles";
import React from "react";
import { Link } from "react-router-dom";
import SortByButton from './components/SortByButton'
import TabList from './components/TabList'
import SearchTagList from './components/SearchTagList'
import "./Header.css";
import { ReactComponent as SearchIcon } from './resources/searchIcon.svg';

function Header(props) {
  function handleKeyPressed(event) {
    console.log(event.key)
    if (event.key == "Enter")
      props.findSearchedPosts(event)
  }
  return (
    <div className="header" key={props.tabs}>

      <div className="header__right">
        <div>
          <div className="afafa">
          <SearchTagList searchTags={props.searchTags} toggleTagByName={props.toggleTagByName} /> 
          </div>
        </div>
      </div>
      <div className="header__left">
        <TabList tabs={props.tabs} changeTabName={props.changeTabName} setTabs={props.setTabs} closeTab={props.closeTab} onClick={props.setTabActive} />
        <div onClick={props.addTabButtonPressed} className="tab-add-button"> </div>
      </div>
      <div style={{marginLeft:"100%"}}>
      <SortByButton sortByDateClicked={props.sortByDateClicked} sortByLikesClicked={props.sortByLikesClicked} sortByTitleClicked={props.sortByTitleClicked} />
      
      </div>
    </div>
  );
}

export default Header;
