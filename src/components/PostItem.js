 import React, { useState } from "react";
import { Tweet } from 'react-twitter-widgets';
import "./PostItem.css";
import SaveToButton from './SaveToButton'
import { Link } from "react-router-dom";
import logo from './uppp.png';
import logo1 from './downnn.png';
import { propTypes } from "react-bootstrap/esm/Image";
import CommentArea from "./CommentArea"

function PostItem(props) {
  const [post, setPost] = useState(props.post);
  const [commentsVisible, setCommentsVisible] = useState(false);
  const [postHidden, setPostHidden] = useState(post.hidden)
  const [postSaved, setPostSaved] = useState(post.saved)
  const [mediaClicked, setMediaClicked] = useState(false)
  const [summaryClicked, setSummaryClicked] = useState(false)
  const [articleClicked, setArticleClicked] = useState(false)
  const title = post.title
  const link = title
  function handleCommentsClicked(){
    setCommentsVisible(!commentsVisible)
  }
  function handleLikeClicked(event) {
    console.log(post.upvote)
    if (post.liked == "liked") return;
    let newPost = JSON.parse(JSON.stringify(post))
    newPost.upvote = newPost.upvote + 1
    if (newPost.liked == "disliked") {
      newPost.liked = "untouched"
    }
    else if (newPost.liked == "untouched") {
      newPost.liked = "liked"
      props.likedPosts.push(newPost)
    }
    let allPostIndex = props.allPosts.indexOf(props.allPosts.find((element) => { return element.title == newPost.title; }))
    props.allPosts[allPostIndex] = newPost
    setPost(newPost)
    console.log("added post ", post)
  }
  function hidePost() {
    setPostHidden(true)
    post.hidden = true

  }
  function handleDislikeClicked(event) {
    console.log(post.upvote)
    if (post.liked == "disliked") return;
    let newPost = JSON.parse(JSON.stringify(post))
    newPost.upvote = newPost.upvote - 1
    if (newPost.liked == "liked") {
      newPost.liked = "untouched"
      props.likedPosts.splice(props.likedPosts.indexOf(post), 1)
    }
    else if (newPost.liked == "untouched") {
      newPost.liked = "disliked"
    }
    let allPostIndex = props.allPosts.indexOf(props.allPosts.find((element) => { return element.title == newPost.title; }))
    props.allPosts[allPostIndex] = newPost
    setPost(newPost)
    console.log("removed post ", post)
  }
  function addPostToSaved(event) {
    props.savedPosts.push(post)
  }
  function removePostFromSaved(event) {
    let postIndex = props.savedPosts.indexOf(props.savedPosts.find((element) => { return element.title == post.title; }))
    props.savedPosts.splice(postIndex, 1)
  }
  function handleSaveClicked(event) {
    if (postSaved == false) addPostToSaved()
    else removePostFromSaved()
    post.saved = !postSaved
    setPostSaved(!postSaved)
  }
  function handleCloseSummaryClicked() {
  	props.removeSummary(post.title + " Summary")
  }
  function handleSummaryClicked(event) {
    if(summaryClicked == false) {
	props.addSummary(post.title + " Summary", post.summary, true, setSummaryClicked)
   	setSummaryClicked(true)
    }
    else {
	props.removeSummary(post.title + " Summary")
	setSummaryClicked(false)
    }
  }
  function GetVideoIframe()
  {
    return(<iframe width="100%" height="420" marginTop="2vh"  src={post.vidUrl} frameborder="0" allowtransparency="true" allowFullScreen="true" allow="encrypted-media"></iframe>)
  }
  function GetNewsIframe()
  {
    return(<iframe width="100%" height="420" marginTop="2vh" src={post.vidUrl} frameborder="0" allowtransparency="true" allowFullScreen="true" allow="encrypted-media"></iframe>)
  }
  function GetTweetEmbed() {
	return(<Tweet tweetId={post.vidUrl}/>)
  }
  function handleCloseMediaClicked() {
  	
  }
  function handleMediaClicked(event) {
    if(mediaClicked == true)
    {
	props.removeSummary(post.title + " Media")
	setMediaClicked(false)
    }
    else {
	props.addSummary(post.title + " Media", post.isVideo? GetVideoIframe() : ((post.for_who=="TWITTER")?GetTweetEmbed():GetNewsIframe()), true, setMediaClicked)
	setMediaClicked(true)
    }
  }
  function handleArticleClicked(event) {
    if(articleClicked == false) {
	props.addSummary(post.title + " Article", post.full_text, true, setArticleClicked)
   	setArticleClicked(true)
    }
    else {
	props.removeSummary(post.title + " Article")
	setSummaryClicked(false)
    }
  }
  function tagClicked(event) {
    props.toggleTagByName(event.target.innerText)
  }
  function GetTags() {
    return (<div id="tagL" className="tagL" style={{fontSize: "x-small" ,display: "flex", color:"#AAAAAA"}}> Tags:&nbsp; {(post.tags == undefined) ? null : post.tags.map((tag) => { return <div id="tagF" style={{color:"#AAAAAA", border:"1px solid #AAAAAA",fontSize: "x-small", borderRadius: "25px", backgroundColor:"inherit", padding: "3px", marginRight: "3px", marginLeft: "3px"}} onClick={tagClicked}> &nbsp;{tag}&nbsp;  </div> })} </div>)
  }
  return (
    <div>
      {postHidden ? null :
        <div className="post" style={{ borderColor: props.borderColor }}>
          <div className="post__left">
            <img src={logo} alt="Site logo" style={{ marginBottom: "0.5vh" }} width="20" height="20" onClick={handleLikeClicked}></img>
            <span>{post.upvote}</span>
            <img src={logo1} alt="Site logo" style={{ marginTop: "0.5vh" }} width="20" height="20" onClick={handleDislikeClicked}></img>
            <span>{post.downvote}</span>
          </div>
          <div className="post__center">
            <img style={{height:"50px", width:"80px", marginBottom: "1vh" }} src={post.image} alt="" />
          </div>
          <div className="post__right">
            <h4>{post.title}</h4>
            <span className="post__info">
              Posted on{" "}
              {post.date}
              &nbsp;for {post.for_who}
            </span>

            <span className="whatever">
              <GetTags />
            </span>
            <p className="post__info1">
              {/* <Link to={`/${post.subreddit}/${link}/comments`}> */}
              <div className="under_tags">
              |&nbsp;&nbsp;&nbsp;&nbsp;
              <a onClick={handleCommentsClicked}> {post.comments_count} Comments&nbsp;&nbsp;&nbsp;&nbsp; </a>
              {/* </Link>{" "} */}
              |&nbsp;&nbsp;&nbsp;&nbsp;Share&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<SaveToButton post={post} addPostToFolder={props.addPostToFolder} folders={props.folders} handleSaveToFavouritesClicked={handleSaveClicked}/>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<a onClick={hidePost} > Hide </a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
              {post.summary == undefined? null : post.summary.length > 1 ? <div> <a onClick={handleSummaryClicked} >{summaryClicked? "Close" : "Summary"}</a> &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; </div>: null}
              {post.full_text == undefined? null: post.full_text.length > 1 ? <div> <a onClick={handleArticleClicked} >{articleClicked? "Close" : "Article"}</a>  &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; </div>: null}
              {post.vidUrl == undefined? null: post.vidUrl.length > 1 ? <div> <a onClick={handleMediaClicked}>{mediaClicked?"Close" : "Open"}</a>  &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; </div>: null}
             
              <a style={{marginLeft: "auto"}} href={post.url} target="_blank">Open on &nbsp;&nbsp;&nbsp;&nbsp;
              <img style={{height:"1.5vh", width:"2vh"}} src={props.srcImg}></img>
              </a>
              </div>
            </p>
          </div>
        </div>}
        {commentsVisible?<CommentArea id={post.title + post.for_who}/>:null}
    </div>
  );
}

export default PostItem;
