import React from "react";

function FolderPath(props)
{
	function handleFolderNameClicked(event)
	{
		props.folderClickedInPath(event.target.innerText)
	}
	return(<div>{props.names.map(name => {return <div onClick={handleFolderNameClicked}>{name}</div>})}</div>)
}
export default FolderPath;