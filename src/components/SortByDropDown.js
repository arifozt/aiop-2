
import React, { useState, useEffect, useRef } from 'react';
import { CSSTransition } from 'react-transition-group';
import { Button} from 'react-bootstrap';
import SortByDropDownOption from "./SortByDropDownOption";
import "./SortByDropDown.css";


{/* 
<div>
			<SortByDropDownOption name="Title" onClick={props.sortByTitleClicked}/>
			<SortByDropDownOption name="Likes" onClick={props.sortByLikesClicked}/>			
      		</div>
        <CSSTransition
         in={activeMenu === 'main'}
         timeout={500}
         classNames="menu-primary"
         unmountOnExit
         onEnter={calcHeight}>
         <div className="menu" onClick = {checkBackButton}>
          <DropdownItem leftIcon = <LeftArrowIcon/> >
            <h2>Profile</h2>
          </DropdownItem>
          <DropdownItem
            leftIcon=<SourcesIcon/>
            goToMenu="Sources">
            Sources
          </DropdownItem>
 */}
function SortByDropDown(props) {
  const [activeMenu, setActiveMenu] = useState('main');
  const [menuHeight, setMenuHeight] = useState(null);
  const dropdownRef = useRef(null);
  useEffect(() => {
    setMenuHeight(dropdownRef.current?.firstChild.offsetHeight)
  }, [])

  function calcHeight(el) {
    const height = el.offsetHeight;
    setMenuHeight(height);
  }
  function handleSortByTitleClicked(event)
  {
    props.turnDropdownOff()
    props.sortByTitleClicked(event)
  }
  function handleSortByLikesClicked(event)
  {
    props.turnDropdownOff()
    props.sortByLikesClicked(event)
  }
  function handleSortByDateClicked(event)
  {
    props.turnDropdownOff()
    props.sortByDateClicked(event)
  }
	return(
        	<div className="dropdown" style={{ height: menuHeight }} ref={dropdownRef}>
			<CSSTransition
         		in={activeMenu === 'main'}
         		timeout={500}
         		classNames="menu-primary"
         		unmountOnExit
         		onEnter={calcHeight}>
         		<div className="menu">
         		
				<SortByDropDownOption name="Title" onClick={handleSortByTitleClicked}/>
				<SortByDropDownOption name="Likes" onClick={handleSortByLikesClicked}/>	
				<SortByDropDownOption name="Date" onClick={handleSortByDateClicked}/>		
			</div>
        	</CSSTransition>
        	</div>
		
	);
				
}

export default SortByDropDown;