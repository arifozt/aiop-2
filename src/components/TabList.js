import React, {useState} from "react";
import TabButton from "./TabButton"
import "./TabList.css"

function TabList(props) {
	return (
    		<div className="tabList">
      			{props.tabs.map(t => <TabButton name={t.name} tabs={props.tabs} changeTabName={props.changeTabName} setTabs={props.setTabs} selected={t.selected} closeTab={props.closeTab} onClick={props.onClick}/>)}
     		</div> 
  		); 
				
}

export default TabList;