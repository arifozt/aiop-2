import React, {useState} from "react";
import FolderItem from "./FolderItem";

function FolderList(props) {

  return (
    <div>
      {
	props.folders.map((folder) =>  { if(JSON.stringify(props.folderPath).localeCompare(JSON.stringify(folder.path)) == 0) return <FolderItem folderPath={props.folderPath} setFolderPath={props.setFolderPath} setFolders={props.setFolders} setPosts={props.setPosts} deleteFolderByName={props.deleteFolderByName} title={folder.title} posts={folder.posts} subfolders={folder.subfolders}  changeFolderName={props.changeFolderName}/> ;})}
    </div>
  );
}

export default FolderList;
