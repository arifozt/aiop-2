import React from "react";
import "./ColorPicker.css";

function ColorPicker(props) {
	return(
		<div>
      			<label>
        			<input className="coco" type="color" value={props.initialColor} id="color-picker" onChange = {props.onChange}/>
      			</label>
    		</div>
	);
				
}

export default ColorPicker;