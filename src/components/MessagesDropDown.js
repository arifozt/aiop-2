
import React, { useState, useEffect, useContext, useRef } from 'react';
import ContactList from "./ContactList"
import MessageBoard from "./MessageBoard"
import {sendMessage} from "../api/api"
import {AppContext} from "../App"
import {isUsername} from "../api/api"
import { CSSTransition } from 'react-transition-group';
import { Button} from 'react-bootstrap';
import "./MessagesDropDown.css";

function MessagesDropDown(props) {
  const context = useContext(AppContext)

  const [activeMenu, setActiveMenu] = useState('main');
  const [menuHeight, setMenuHeight] = useState(null);
  const [contacts, setContacts] = [context.allContacts, context.setAllContacts]
  const [selectedChat, setSelectedChat] = useState(-1)
  const dropdownRef = useRef(null);

  useEffect(() => {
	var newMessages = JSON.parse(JSON.stringify(context.globalMessages))
	if(selectedChat == -1 || selectedChat >= contacts.length) return
	var messageIndex = -1
	var messagesChanged = 0
	for(const message of context.messages[contacts[selectedChat].name]) { 
		messageIndex++
		if(message.seen == true) continue
		messagesChanged++
		newMessages[contacts[selectedChat].name][messageIndex].seen = true
		console.log("SOME BUG PRE ", message)
		sendMessage({message: message.message, sender: context.username, receiver: message.converser, time:message.time, sent: message.sent, seen: true, seenFlag: true})
	}
	if(messagesChanged == 0) return
	console.log("CHECK NEW MESSAGES", newMessages)
	context.setGlobalMessages(JSON.parse(JSON.stringify(newMessages)))
	context.setMessages(newMessages)
  }, [selectedChat, context.messages])  

  useEffect(() => {
    setMenuHeight(dropdownRef.current?.firstChild.offsetHeight)
	setMenuHeight(1000)
  }, [])

  function calcHeight(el) {
    const height = el.offsetHeight;
    setMenuHeight(height);
  }

  function handleNewChatClicked(event) {
  	setActiveMenu('newChats')
  }

  function handleBackInNewChatClicked(event) {
  	setActiveMenu('main')
  }

  function handleContactClicked(event) {
	const clickedContactName = event.target.innerText
	var maxMatch = 0, maxMatchIndex = 0;
	console.log("CHECK UNSEEN 1", clickedContactName)
	for(var i=0;i<contacts.length;i++) {
		if(clickedContactName.includes(contacts[i].name) && maxMatch < contacts[i].name.length) {
			maxMatch = contacts[i].name.length
			maxMatchIndex = i
		}
	}
	console.log("CHECK UNSEEN 1", clickedContactName, maxMatch, maxMatchIndex)
	setSelectedChat(maxMatchIndex)
  }

  function handleUserKeyPressed(event) {
	if(event.key == 'Enter') {
		const addedUsername = event.target.value
		if(isUsername({username: addedUsername}))
		{
			console.log("CHAT BUG", contacts)
			var newContacts = JSON.parse(JSON.stringify(contacts))
			newContacts.push({"name": addedUsername})
			setContacts(newContacts)
			context.setAllContacts(context.allContacts.concat([{"name": addedUsername}]))
		}
	}
  }

  console.log("DROPDOWN MESSAGE BUG")
	return(
        	<div style={{marginBottom:"1vh",marginLeft:"2vh", width:"20vh",color: "white",height: "40vh" }} ref={dropdownRef}>
			<CSSTransition
         		in={activeMenu === 'main'}
         		timeout={500}
         		classNames="menu-primary"
         		unmountOnExit
         		onEnter={calcHeight}>
         		<div className="menu">
			{context.logged? <div>	
				<div style={{marginLeft:"13vh"}}><button className='button-28' style={{width:"fit-content",height:"fit-content",marginLeft:"auto", backgroundColor:"inherit",border:"1px solid white",color:"#E4DCBE", borderRadius:"25px"}} onClick={handleNewChatClicked}> + </button></div>

			<div style={{fontSize:"x-small"}}>Contacts :</div>
			<ContactList contacts={contacts} unseenMessages={context.unseenMessages} onContactClick={handleContactClicked}/>
			<MessageBoard selectedChat={selectedChat} contacts={contacts} sender={context.username} messages={context.messages} setMessages={context.setMessages} getMessages={context.getMessages} setGlobalMessages={context.setGlobalMessages} queuedMessages={context.queuedMessages} setQueuedMessages={context.setQueuedMessages} globalQueuedMessages={context.globalQueuedMessages} setGlobalQueuedMessages={context.setGlobalQueuedMessages} getMessagesNo={context.getMessagesNo} setMessagesNo={context.setMessagesNo} unseenMessages={context.unseenMessages} setUnseenMessages={context.setUnseenMessages}/>
			</div> : <div> Log in to use messages! </div>}
			</div>
        	</CSSTransition>
			<CSSTransition
         		in={activeMenu === 'newChats'}
         		timeout={500}
         		classNames="menu-primary"
         		unmountOnExit
         		onEnter={calcHeight}>
         		<div className="menu">
         		<h4>New Chat</h4>
			<input style={{marginTop:"2vh",marginBottom:"2vh",backgroundColor:"inherit", border:"1px solid white", padding:"1vh", borderRadius:"25px", maxWidth:"13vh"}} type="text" id="newChatInput" placeholder="Type username..." onKeyPress={handleUserKeyPressed}/>
			<div><button className='button-28' onClick={handleBackInNewChatClicked}> Back </button></div>
			</div>
        	</CSSTransition>
			
        	</div>
		
	);
				
}

export default MessagesDropDown;