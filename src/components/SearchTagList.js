import React from "react";
import SearchTag from './SearchTag'
import "./SearchTagList.css";


function SearchTagList(props) {
	return (
    		<div classname="scar">
      			{props.searchTags == undefined? null: props.searchTags.map(s => <SearchTag name={s.name} toggleTagByName={props.toggleTagByName} />)}
     		</div> 
  		); 
				
}

export default SearchTagList;