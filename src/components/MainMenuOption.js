import React, {useState} from "react";
import "./MainMenuOption.css";

function MainMenuOption(props) {
  const [visible, setVisible] = useState(false)
  function changeOptionVisibility(event)
  {
	setVisible(!visible)
  }
  return (
    <div  className={`mainMenuOption ${props.active && "mainMenuOption--active"}`}>
	<div className="mainMenuOptionContent" onClick = {(props.onClick == undefined)? changeOptionVisibility : props.onClick}>      
		<props.Icon />
      		<h2 className="mainMenuWoman">{props.text}</h2>
	</div>
      {visible? props.children: null}
    </div>
  );
}

export default MainMenuOption;
