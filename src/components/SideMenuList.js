import React, {useState, useEffect} from "react";
import SideMenu from "./SideMenu";
import './SideMenuList.css'

function SideMenuList(props) {
	const [scrollValue, setScrollValue] = useState(0)
	useEffect(() => {console.log("FOCUS BUG EFFECT"); document.getElementsByClassName("sideMenuList")[0].scrollTo(scrollValue,scrollValue)},[])
	function handleSideMenuScrolled(event)
	{
		console.log("SCROLLED IN FOCUS BUG", event.target.scrollTop)
		setScrollValue(event.target.scrollTop)
	}
	return(
	<div className="sideMenuList" id="sideMenuScroll" onScroll={handleSideMenuScrolled}>
		{
			
			props.sideMenus.map(sideMenu => <SideMenu visible={sideMenu.visible} setVisible={sideMenu.setVisible} title={sideMenu.title} body={sideMenu.body}/>)
		}
	</div>
	)
}

export default SideMenuList