import React, {useState} from "react";
import "./SideMenu.css";

function SideMenu(props) {
	const [visible, setVisible] = useState(props.visible)
	function handleXClicked(){
		setVisible(false)
		props.setVisible(props.title)
	}

	return(<div> {props.visible? (<div className="sideMenu__container">
			<div className="only">
			<button className="xxbutton" onClick={handleXClicked}>x</button><h4>{props.title}</h4>
			</div>
					<br></br>
				{props.body} 
				{props.children}
      			   </div>) 
			   : (<div></div>)
			} </div>);
	
}
export default SideMenu			
