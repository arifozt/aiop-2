import React, {useState} from "react";
import "./SortByDropDownOption.css";
import {ReactComponent as UpIcon} from '../resources/up-arrow.svg'

function SortByDropDownOption(props) {
	const [iconState, setIconState] = useState("untouched")
	function handleOptionClicked(event)
	{
		if(iconState == "untouched") setIconState("ascending")
		else if(iconState == "ascending") setIconState("descending")
		else setIconState("untouched")			
		props.onClick(event, iconState)
	}
	function GetIcon()
	{
		return ( <div className="icon"> { (iconState == "untouched")? ">  " : ((iconState == "descending")? <UpIcon style={{width: "1vh", height: "2vh"}}/>:<UpIcon style={{transform: "rotateX(180deg)", width: "1vh", height: "2vh"}}/>)} </div>);
	}
	return(
		<div onClick={handleOptionClicked} className="menu-item">
			<GetIcon/>
			{props.name}			
      		</div>
	);
				
}

export default SortByDropDownOption;