import React, { useState, useEffect } from "react";
import PostList from './PostList'
import FolderList from './FolderList'
import FolderPath from './FolderPath'
import "./Feed.css";

function Feed(props) {

	const [selectedPosts, setSelectedPosts] = [props.posts, props.selectedPosts]
	return (
		<div id="feed" className="feed" >
			{props.inFolders ? <FolderPath names={props.folderPath} folderClickedInPath={props.folderClickedInPath} /> : null}
			{props.inFolders ? <button className="button-29" onClick={props.makeNewFolder}>Add Folder</button> : null}
			{props.inFolders ? <FolderList folderPath={props.folderPath} setFolderPath={props.setFolderPath} setFolders={props.setFolders} setPosts={props.setPosts} deleteFolderByName={props.deleteFolderByName} changeFolderName={props.changeFolderName} folders={props.folders} /> : null}
			<PostList removeSummary={props.removeSummary} addSummary={props.addSummary} setVideosVisible={props.setVideosVisible} setVideosBody={props.setVideosBody} key={selectedPosts} addPostToFolder={props.addPostToFolder} folders={props.folders} activeTabIndex={props.activeTabIndex} toggleTagByName={props.toggleTagByName} allPosts={props.allPosts} posts={props.posts} setPosts={props.setPosts} history={props.history} likedPosts={props.likedPosts} savedPosts={props.savedPosts} setSummaryVisible={props.setSummaryVisible} setSummaryBody={props.setSummaryBody} />
		</div>
	);
}

export default Feed;