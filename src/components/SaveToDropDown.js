
import React, { useState, useEffect, useRef } from 'react';
import { CSSTransition } from 'react-transition-group';
import { Button} from 'react-bootstrap';
import SaveToDropDownOption from "./SaveToDropDownOption";
import "./SaveToDropDown.css";


{/* 
<div>
			<SaveToDropDownOption name="Favourites">
			<SaveToDropDownOption name="Folders">			
      		</div>
        <CSSTransition
         in={activeMenu === 'main'}
         timeout={500}
         classNames="menu-primary"
         unmountOnExit
         onEnter={calcHeight}>
         <div className="menu" onClick = {checkBackButton}>
          <DropdownItem leftIcon = <LeftArrowIcon/> >
            <h2>Profile</h2>
          </DropdownItem>
          <DropdownItem
            leftIcon=<SourcesIcon/>
            goToMenu="Sources">
            Sources
          </DropdownItem>
 */}
function SaveToDropDown(props) {
  const [activeMenu, setActiveMenu] = useState('main');
  const [menuHeight, setMenuHeight] = useState(null);
  const [activePath, setActivePath] = useState([])
  const dropdownRef = useRef(null);
  useEffect(() => {
    setMenuHeight(dropdownRef.current?.firstChild.offsetHeight)
  }, [])

  function calcHeight(el) {
    const height = el.offsetHeight;
    setMenuHeight(height);
  }
  function handleFoldersClicked(event) {
  	setActiveMenu('main-folders')
  }
  function handleFoldersBackClicked(event) {
	if(activePath.length == 0)
  		setActiveMenu('main')
  	else {
		var newPath = JSON.parse(JSON.stringify(activePath))
		newPath.splice(newPath.length-1, 1)
		setActivePath(newPath)
	}
  }
  function handleFolderClicked(event) {
	props.setDropDownVisible(false)
  	props.addPostToFolder(event.target.innerText, props.post)
  }
  function handleFolderIconClicked(event) {
  	var newPath = JSON.parse(JSON.stringify(activePath))
	console.log(event.target.id)
	newPath.push(event.target.id)
	setActivePath(newPath)
  }
	return(
        	<div className="dropdown" style={{ height: menuHeight }} ref={dropdownRef}>
			<CSSTransition
         		in={activeMenu === 'main'}
         		timeout={500}
         		classNames="menu-primary"
         		unmountOnExit
         		onEnter={calcHeight}>
         		<div className="menu">
         		
				<SaveToDropDownOption name="Favourites" onClick={handleFolderClicked}/>
				<SaveToDropDownOption name="Folders" onClick={handleFoldersClicked}/>			
			</div>
        	</CSSTransition>
			<CSSTransition
         		in={activeMenu === 'main-folders'}
         		timeout={500}
         		classNames="menu-secondary"
         		unmountOnExit
         		onEnter={calcHeight}>
         		<div className="menu">
         			{props.folders.map( (folder) => { if(JSON.stringify(folder.path).localeCompare(JSON.stringify(activePath)) == 0) return <SaveToDropDownOption name={folder.title} onClick={handleFolderClicked} onIconClick={handleFolderIconClicked}/> })}
				<SaveToDropDownOption name="Back" onClick={handleFoldersBackClicked}/>		
			</div>
        	</CSSTransition>
        	</div>
		
	);
				
}

export default SaveToDropDown;