
import React, { useState, useEffect, useContext, useRef } from 'react';
import {AppContext} from "../App"
import { CSSTransition } from 'react-transition-group';
import { Button} from 'react-bootstrap';
import "./MessagesDropDownButton.css";
import MessagesDropDown from "./MessagesDropDown"
import Draggable from "react-draggable";

function MessagesDropDownButton(props) {
  const context = useContext(AppContext)

  const [messagesVisible, setMessagesVisible] = useState(false)
  const [unseenMessagesTotal, setUnseenMessagesTotal] = useState(0)
 
  useEffect(() => {
  	var newUnseenMessagesTotal = 0
	for(const key of Object.keys(context.unseenMessages)) {
		newUnseenMessagesTotal += context.unseenMessages[key]
	}
	setUnseenMessagesTotal(newUnseenMessagesTotal)
  }, [context.unseenMessages])

  function handleMessagesClicked(event) {
	  setMessagesVisible(!messagesVisible)
  }
  
  console.log("DROPDOWN MESSAGE BUG")
	return(
		<Draggable>
		<div className='Messages'>
			
        	<button style={{marginBottom:"1vh", maxWidth:"10vh",color:"white",backgroundColor:"inherit" ,border:"1px solid white",borderRadius:"25px", padding:"1vh",paddingBottom:"1vh"}} onClick={handleMessagesClicked}>Chat {unseenMessagesTotal}</button>
			<div style={{border:"1px solid white"}}>{messagesVisible? <MessagesDropDown/> : null}</div>
		</div>
		</Draggable>
	);
				
}

export default MessagesDropDownButton;