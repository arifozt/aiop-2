import React, { useState, useContext } from "react";
import {AppContext} from "../App"
import {firebaseApi} from "../api/firebaseApi";
import "./MainMenu.css";
import TwitterIcon from "@material-ui/icons/Twitter";
import FolderIcon from '@material-ui/icons/ArchiveOutlined';
import MainMenuOption from "./MainMenuOption";
import SubMainMenuOption from "./SubMainMenuOption";
import HomeIcon from "@material-ui/icons/HomeOutlined";
import SearchIcon from "@material-ui/icons/Search";
import NotificationsNoneIcon from "@material-ui/icons/NotificationsNone";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import BookmarkBorderIcon from "@material-ui/icons/History";
import ListAltIcon from "@material-ui/icons/FavoriteBorder";
import PermIdentityIcon from "@material-ui/icons/PermIdentity";
import tags from "@material-ui/icons/BookmarkBorderOutlined";
import souurces from "@material-ui/icons/ChromeReaderModeOutlined";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import { Button } from "@material-ui/core";
import SideMenu from "./SideMenu"
import TagList from "./TagList"
import { ReactComponent as SettingsWheelIcon } from '../resources/settingsWheel.svg'
import SearchOptions from './SearchOptions'
import logo from './scoopt4.png';
import { data } from 'jquery';
import * as $ from 'jquery';

function MainMenu(props) {

  const context = useContext(AppContext)

  const [loginErrorMessage, setLoginErrorMessage] = useState("")
  const [filtersVisible, setFiltersVisible] = useState(false)
  const [registerFocused, setRegisterFocused] = useState(false)
  const [inFolders, setInFolders] = [props.inFolders, props.setInFolders]
  const [logged, setLogged] = [context.logged, context.setLogged]
  const [username, setUsername] = [context.username, context.setUsername]
  
  function filterMenuClicked(event) {
    setFiltersVisible(!filtersVisible)
  }

  function handleFoldersClicked(event) {
    setInFolders(!inFolders)
    props.setFoldersToFeed()
  }

  function handleHomeClicked(event) {
    setInFolders(false)
    props.setAllAsPosts()
  }

  function handleFavouritesClicked(event) {
    setInFolders(false)
    props.setSavedAsPosts()
  }

  function handleHistoryClicked(event) {
    setInFolders(false)
    props.setHistoryAsPosts()
  }

  function handleKeyPressed(event) {
    if (event.key == "Enter")
      props.findSearchedPosts(event)
  }

  function handleTagsClicked(event) {
    // props.setTagsVisible(true)
  }

  function handleSourcesClicked(event) {
    props.setSourcesVisible(true)
  }

  function handleLikedClicked(event) {
    setInFolders(false)
    props.setLikedAsPosts()
  }

  function GetFiltersBody() {
    return (
      <div>Search in:</div>)

  }

  function handleRegisterClicked() {
    setLoginErrorMessage("")
    setRegisterFocused(!registerFocused)
  }
  function registerUser() {
    let username = document.getElementById('usernameInput').value
    let password = document.getElementById('passwordInput').value
    let email = document.getElementById('emailInput').value 
		const db = firebaseApi.firestore()
		db.collection('user data').doc(username).get().then(userData => {
			if(userData["Df"] != null) throw new Error("user already taken")
			db.collection('user data').doc(username).set({"email": email})
			firebaseApi.auth().createUserWithEmailAndPassword(email, password).then(data => {}).catch((error) => {setLoginErrorMessage(error.message)})
		}).catch((error) => {setLoginErrorMessage(error.message)})
	
  }

  function loginUser() {
    let username = document.getElementById('usernameInput').value
    let password = document.getElementById('passwordInput').value
    const db = firebaseApi.firestore()
    db.collection('user data').doc(username).get().then(userData => {
    	if(userData["Df"] == null) throw new Error("Username not found")
	let email = userData["Df"]["sn"]["proto"]["mapValue"]["fields"]["email"]["stringValue"]
        firebaseApi.auth().signInWithEmailAndPassword(email, password).then(data => { console.log("LOGGED IN");setUsername(username); context.setEmail(email); setLogged(true);}).catch((error) => {setLoginErrorMessage(error.message)})
    }).catch((error) => {setLoginErrorMessage(error.message)})
  }

  function handleConfirmClicked(event) {
    setLoginErrorMessage("")
    if (registerFocused) registerUser()
    else loginUser()
  }

  function logoutUser(event) {
    firebaseApi.auth().signOut().then(() => { context.setLogged(false) })
  }

  function handleClearClicked(event) {
    localStorage.clear()
  }
  return (
    <div id="all" className="mainMenu">
      {/* <TwitterIcon className="sidebar__twitterIcon" /> */}
      {/* <MainMenuOption active Icon={HomeIcon} text="Home" /> */}
      <div id="mainmenuu">
        <MainMenuOption Icon={HomeIcon} text="Home" onClick={handleHomeClicked} />
        {/* <SubMainMenuOption Icon=<SettingsWheelIcon style={{width: "4vh", height: "4vh"}}/> text="Filters" onClick={filterMenuClicked}/>*/}
        {/*<div> {filtersVisible? <SearchOptions searchInTitle = {props.searchInTitle} setSearchInTitle = {props.setSearchInTitle} searchInBody = {props.searchInBody} setSearchInBody = {props.setSearchInBody} searchInTags = {props.searchInTags} setSearchInTags = {props.setSearchInTags} searchInAuthors = {props.searchInAuthors} setSearchInAuthors = {props.setSearchInAuthors}/> : null } </div>  */}
        <MainMenuOption Icon={BookmarkBorderIcon} text="History" onClick={handleHistoryClicked} />
        <MainMenuOption Icon={FolderIcon} text="Saved" onClick={handleFoldersClicked} />
        <MainMenuOption Icon={ListAltIcon} text="Liked" onClick={handleLikedClicked} />
        <MainMenuOption Icon={tags} text="Tags">
          <input type="text" placeholder="Type to Search..." size="12" className="leftSearch" name="tag-search" onChange={props.handleTagSearchChanged} /><SearchIcon style={{ width: "2vh", height: "2vh", marginLeft: "1vh" }} />
          <TagList tags={props.tags} toggleTagByName={props.toggleTagByName} handleTagChange={props.handleTagChange}/>
        </MainMenuOption>
        {!props.sourcesVisible ? <MainMenuOption Icon={souurces} text="Sources" onClick={handleSourcesClicked} /> : null}
        <MainMenuOption Icon={PermIdentityIcon} text={logged ? "Logged In" : "Account"}>
          {!logged ? <div>
            <br></br>
            <input  className="userName" id="assa" placeholder="Username" type="text" id="usernameInput" required />
            <br></br>
            <br></br>
            <input className="passWord" id="aaas" placeholder="Password" type="password" id="passwordInput" required />
            <br></br>
            <br></br>
            {registerFocused ? <input className="email" type="text" placeholder="Email" id="emailInput" required /> : null}
            <br></br>
            {registerFocused ? <button className="button-28" role="button" onClick={handleConfirmClicked}> Confirm </button> : <button class="button-28" role="button" onClick={handleConfirmClicked}> Log in </button>}

            <button className="button-28" role="button" onClick={handleRegisterClicked}> {registerFocused ? "Back" : "Register"} </button> {loginErrorMessage} </div> : <button class="button-33" role="button" onClick={logoutUser}> Log Out </button>}
        </MainMenuOption>
        <MainMenuOption Icon={MoreHorizIcon} text="More">
          <SubMainMenuOption Icon=<SettingsWheelIcon style={{ width: "4vh", height: "4vh" }} /> text="Dislikes" onClick={props.setDislikedAsPosts}/>
          <SubMainMenuOption Icon=<SettingsWheelIcon style={{ width: "4vh", height: "4vh" }} /> text="Hidden" onClick={props.setHiddenAsPosts}/>
          <SubMainMenuOption Icon=<SettingsWheelIcon style={{ width: "4vh", height: "4vh" }} /> text="ClearLC" onClick={handleClearClicked}/>
        </MainMenuOption>
      </div>
      {/* Button -> Tweet}
      {/* <Button variant="outlined" className="mainMenu__tweet" fullWidth>
        Create Recipe
      </Button> */}
    </div>
  );
}

export default MainMenu;
