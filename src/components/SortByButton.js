import React, {useState} from "react";
import SortByDropDown from './SortByDropDown';
import {ReactComponent as SortByIcon} from "../resources/sortBy.svg"
import "./SortByButton.css";

function SortByButton(props) {
	const [sortByDropDownVisible, setSortByDropDownVisible] = useState(false)
	function handleSortByButtonClicked(event)
	{
		setSortByDropDownVisible(!sortByDropDownVisible)
	}
	function turnDropdownOff()
	{
		setSortByDropDownVisible(false)
	}
	return(
		<div className="sortByButton">
			<button className="button-28" style={{width:"fit-content"}} onClick={handleSortByButtonClicked}/>
			{sortByDropDownVisible? <SortByDropDown turnDropdownOff={turnDropdownOff}sortByDateClicked = {props.sortByDateClicked} sortByLikesClicked = {props.sortByLikesClicked} sortByTitleClicked={props.sortByTitleClicked}/> : null}			
      		</div>
	);
				
}

export default SortByButton;