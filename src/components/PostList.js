import React, {useEffect, useState, useContext} from "react";
import {AppContext} from "../App"
import "./PostList.css";
import PostItem from "./PostItem";

var img_src = {}
function PostList(props) {
  const context = useContext(AppContext)
  const selectedSources = context.allSources.filter(source=>source.selected)
  const allSources = context.allSources
  function GetFirstPageEmptyFeed()
  {
    return(<div className="picture"> </div>)
  }
  function GetAnyPageEmptyFeed()
  {
    return(<div className="tab"></div>)
  }
  useEffect(() => {
    allSources.forEach(element => {
      img_src[element.name] = element.img
    })});
function GetEmptyFeed(){
  return (<div >{props.activeTabIndex == 0? <GetFirstPageEmptyFeed/>:<GetAnyPageEmptyFeed/>} </div>)
}

  return (
    <div className="posts">
      {props.posts.length == 0 ? <GetEmptyFeed />: 
      props.posts.map((post) =>  { return <PostItem removeSummary={props.removeSummary} addSummary={props.addSummary} srcImg={(selectedSources.find((source) => {return source.name.toLowerCase().localeCompare(post.for_who.toLowerCase()) == 0}) == undefined)? "": (selectedSources.find((source) => {return source.name.toLowerCase().localeCompare(post.for_who.toLowerCase()) == 0 }).img)} setVideosVisible={props.setVideosVisible} setVideosBody={props.setVideosBody} addPostToFolder={props.addPostToFolder} folders={props.folders} toggleTagByName = {props.toggleTagByName} allPosts = {props.allPosts} savedPosts = {props.savedPosts} post={post} history={props.history} likedPosts = {props.likedPosts} setSummaryVisible = {props.setSummaryVisible} setSummaryBody = {props.setSummaryBody} borderColor={(selectedSources.find((source) => {return source.name.toLowerCase().localeCompare(post.for_who.toLowerCase()) == 0}) == undefined)? "black": (selectedSources.find((source) => {return source.name.toLowerCase().localeCompare(post.for_who.toLowerCase()) == 0 }).color)}/>;})}
    </div>
  );
}

export default PostList;
