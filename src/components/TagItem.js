import React, {useState} from "react";
import "./TagItem.css";

function TagItem(props) {
        const [selected, setSelected] = useState(props.selected)
	function handleTagClicked(event)
        {
		props.toggleTagByName(event.target.innerText)
	}
	return(
		<div className="tagItem"  id = {'tag-item'+props.name} onClick={handleTagClicked}>
			<div className="tagName">{props.name}</div>		
      		</div>
	);
				
}

export default TagItem;