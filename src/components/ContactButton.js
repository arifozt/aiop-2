import React from "react";
import "./ContactButton.css";

function ContactButton(props) {
	return(<div classname="contactButton" onClick={props.onClick}> {props.name} <br></br> {props.unseenMessages} </div>);
	
}
export default ContactButton		
