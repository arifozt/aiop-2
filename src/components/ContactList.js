import React from "react";
import ContactButton from "./ContactButton";
import './ContactList.css'

function ContactList(props) {
	return(
	<div className="contactList">
		{	
			props.contacts.map(contact => <ContactButton name={contact.name} unseenMessages={Array.from(Object.keys(props.unseenMessages)).includes(contact.name)? props.unseenMessages[contact.name]: 0} onClick={props.onContactClick}/>)
		}
	</div>
	)
}

export default ContactList