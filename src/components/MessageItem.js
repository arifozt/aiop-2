import React from "react";
function MessageItem(props) {
	return(
		<div style={{fontSize:"x-small",padding:"0.7vh", textAlign:"left",marginBottom:"0.5vh",marginRight:"2vh", marginLeft: (props.sent) ? "auto" : "0px", width: "fit-content",textAlign: (props.sent) ? "right" : "left",border: (props.sent) ? "1px solid #1ED45F" : "1px solid #FF4300", borderRadius:"15px" ,backgroundColor: (props.sent) ? "#373737" : "#181818"}}>
      			{props.message}
    		</div>
	);
				
}

export default MessageItem;