import React, { useState } from "react";
import "./SourceAccountButton.css";
import PermIdentityIcon from "@material-ui/icons/PermIdentityOutlined";

function SourceAccountButton(props) {
	const CLIENT_ID = "c821239be6c64495a76be6f30d195c85"
	const REDIRECT_URI = "http://localhost:3000"
	const AUTH_ENDPOINT = "https://accounts.spotify.com/authorize"
	const RESPONSE_TYPE = "token"
	return (
		<a href={`${AUTH_ENDPOINT}?client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&response_type=${RESPONSE_TYPE}`}>
			<button className="buttonInSources" onClick={props.onClick}></button>
		</a>
	);

}

export default SourceAccountButton;

// BQAyGOD0kq5DCyMCNJ4YQ9ZGMmtziSP3VS06ZfbCy-wohvTdIBBzdaIIANpOIW2LhwUfy2xRz5L6wAn1nE7TRZSPXgZlMH8ce3ubT8BX6_NdKiISq_m_UKT28BquPp7iUNRd_ZkSLo2Eb7e0xAgQAWHfynCw8hdmcB8