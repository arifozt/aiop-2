import React, { useState, useEffect, useContext } from "react";
import {AppContext} from '../App'
import "./CommentArea.css"
import {firebaseApi} from '../api/firebaseApi'
import PermIdentityIcon from "@material-ui/icons/PermIdentity";
import logo from './uppp.png';
import logo1 from './downnn.png';

function CommentArea(props) {
  const context = useContext(AppContext)
  const [comments, setComments] = useState([])
  useEffect(() => {
    const db = firebaseApi.firestore()	
    db.collection('Comments').doc(props.id).get().then(snapshot => {
      var apiComments = []
      console.log("BUG COMMENTS API", )
      if(snapshot["Df"] == undefined) return
      snapshot["Df"]["sn"]["proto"]["mapValue"]["fields"]["newComments"]["arrayValue"]["values"].forEach(comment => {
        apiComments.push({value: comment["mapValue"]["fields"]["value"]["stringValue"], date: comment["mapValue"]["fields"]["date"]["stringValue"], user: comment["mapValue"]["fields"]["user"]["stringValue"]})
      })
      setComments(comments.concat(apiComments))
    })
  }, [])
  function handleCommentKeyPressed(event)
  {
    if(event.key=='Enter') {
      console.log("BUG COMMENTS", document.getElementById("input " + props.id).value)
        var newComments = JSON.parse(JSON.stringify(comments))
        var currentdate = new Date().toLocaleString();
        console.log(currentdate)
        newComments.push({value: document.getElementById("input " + props.id).value, date: currentdate, user: context.username})
        setComments(newComments)
        const db = firebaseApi.firestore()	
        db.collection('Comments').doc(props.id).set({newComments})
    }
    console.log(comments, "BUG COMMENTS")
  }
  return(
    <div className="helloe">
      
      <input placeholder="Write a comment..." className="inputComment" type="text" onKeyPress={handleCommentKeyPressed} id={"input " + props.id}></input>
      
      {comments.map(comment => <div style={{padding: "10px", border:"1px solid #E4DCBE", borderRadius: "25px", margin: "1vh", overflowX:"show", width:"50vh", marginLeft:"5vh"}}><div className="comUserNDate"><div className="comUser">{comment.user} <div className="comDate">{comment.date}</div></div></div>  <div className="comVal">{comment.value}</div>  </div>)}
      
    </div>
  )
}
export default CommentArea