import React, {useState} from "react";
import "./SourceItem.css";
import SourceAccountButton from "./SourceAccountButton"
import ColorPicker from "./ColorPicker.js"
import { ReactComponent as LeftIcon } from "../resources/arrow.svg"
import { ReactComponent as TickIcon } from "../resources/tick.svg"

function SourceItem(props) {
	const [borderColor, setBorderColor] = useState(props.color)
	function handleColorChanged(event)
	{
    		let element = document.getElementById('source-item'+props.name)
		setBorderColor(event.target.value)
		element.style.borderColor = event.target.value;
		let pickedSource = element.innerText;
		if(pickedSource.includes('\n'))
			pickedSource = pickedSource.slice(2)
		const pickedElement = props.sources.find((element) => {return element.name == pickedSource;});
		let newSources = Array.from(props.sources)
		const newSourcesIndex = newSources.indexOf(pickedElement)
		newSources[newSourcesIndex].color = event.target.value
		props.setSources(newSources)
		props.updatePosts()
	}
	function handleIconClicked()
	{
		props.onClick({target:{innerText: props.name}})
	}
	return(
		<div className="sourceItem" id = {'source-item'+props.name} style = {{borderColor: props.color }} onClick={props.onClick}>
        		<img style={{height:"30px", width:"40px"}} src={props.img}></img>
			<div className="sourceName" >{props.name}</div>
			{!('A'<=props.icon&&props.icon<='Z') ? <div><ColorPicker initialColor = {borderColor} onChange = {handleColorChanged}/><SourceAccountButton/></div> : null}			
			<div className="icon-button" onClick={handleIconClicked}>{'A'<=props.icon&&props.icon<='Z'? <div className="red" style={{'backgroundColor': '#fe0000', 'width': "4.5px", 'height': "4.5px", 'borderRadius': "25px"}}/> : <div style={{'backgroundColor': '#48FE4C', 'width': "4.5px", 'height': "4.5px", 'borderRadius': "25px"}}/>}</div> 
      		</div>
	);
				
}

export default SourceItem;