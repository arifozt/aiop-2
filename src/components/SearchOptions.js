import React from "react";

function SearchOptions(props) {
  function handleTitleChecked(event)
  {
	props.setSearchInTitle(!props.searchInTitle)
  }
  function handleBodyChecked(event)
  {
	props.setSearchInBody(!props.searchInBody)
  }
  function handleTagsChecked(event)
  {
	props.setSearchInTags(!props.searchInTags)
  }
  function handleAuthorsChecked(event)
  {
	props.setSearchInAuthors(!props.searchInAuthors)
  }
  return (
    <div>
	Search in:
	<br></br>
	<label> <input type="checkbox" id="cb_title" defaultChecked={true} onChange={handleTitleChecked}/> Title </label>
	<label> <input type="checkbox" id="cb_body" defaultChecked={true} onChange={handleBodyChecked}/> Body </label>
	<br></br>
	<label> <input type="checkbox" id="cb_tags" defaultChecked={true} onChange={handleTagsChecked}/> Tags </label>
	<label> <input type="checkbox" id="cb_authors" defaultChecked={true} onChange={handleAuthorsChecked}/> Authors </label>
    </div>
  );
}

export default SearchOptions;
