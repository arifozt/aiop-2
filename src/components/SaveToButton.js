import React, {useState} from "react";
import SaveToDropDown from './SaveToDropDown';

function SaveToButton(props) {
	const [SaveToDropDownVisible, setSaveToDropDownVisible] = useState(false)
	function handleSaveToButtonClicked(event)
	{
		setSaveToDropDownVisible(!SaveToDropDownVisible)
	}
	return(
		<div>
			<div style={{color:"#ff8686"}} onClick={handleSaveToButtonClicked}> Save </div>
			{SaveToDropDownVisible? <SaveToDropDown addPostToFolder={props.addPostToFolder} post={props.post} folders={props.folders} dropDownVisible={SaveToDropDownVisible} setDropDownVisible={setSaveToDropDownVisible} handleSaveToFavouritesClicked={props.handleSaveToFavouritesClicked}/> : null}			
      		</div>
	);
				
}

export default SaveToButton;