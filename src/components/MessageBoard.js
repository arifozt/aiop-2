import React, {useState, useEffect} from "react";
import {sendMessage} from "../api/api"
import MessageList from "./MessageList"
var searchTerm = ""
function MessageBoard(props) {
	const [messages, setMessages] = [props.messages, props.setMessages]
	const [unseenMessages, setUnseenMessages] = [props.unseenMessages, props.setUnseenMessages]
	const [getMessagesNo, setMessagesNo] = [props.getMessagesNo, props.setMessagesNo]

	function onKeyPress(event) {
		if(event.key == 'Enter') {
			console.log("MESAAGE ", searchTerm)
			console.log("FAOO",props.contacts[props.selectedChat].name)
			var newMessage = {message: event.target.value, sender: props.sender, receiver: props.contacts[props.selectedChat].name, sent: true, seen: true}
			setMessagesNo(getMessagesNo() + 1)
			console.log("CHECK BUG", props.getMessages())
			var newMessages = JSON.parse(JSON.stringify(props.getMessages()))
			newMessages[newMessage.receiver].push(newMessage)
			setMessages(newMessages)
			props.setGlobalMessages(JSON.parse(JSON.stringify(newMessages)))
			sendMessage(newMessage)
			event.target.value = ""
		}
		else if(event.key == 'Backspace') {
			searchTerm = searchTerm.substring(0, searchTerm.length - 1)
		}
		else searchTerm = searchTerm.concat(event.key)
	}

	return(
		<div>
      			{props.selectedChat == -1? " " : <div>
										{props.messages[props.contacts[props.selectedChat].name] == null? <div/> : <div style={{marginBottom:"1vh"}}>
										 <MessageList messages={props.messages[props.contacts[props.selectedChat].name]}/> </div>}
										 <input style={{maxWidth:"13vh" ,color:"white",padding:"1vh",marginBottom:"2vh", backgroundColor:"inherit", border:"1px solid grey"}} type="text" placeholder="Type message..." onKeyPress={onKeyPress}/> 
									       </div>}
    		</div>
	);
				
}

export default MessageBoard;