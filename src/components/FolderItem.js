import React, { useState } from "react";
import "./FolderItem.css";
function FolderItem(props) {
  const [nameSet, setNameSet] = useState(true)
  function handleNameClicked(event) {
    event.stopPropagation()
    setNameSet(false)
  }
  function handleNameChange(event) {
    if (event.key == 'Enter') {
      props.changeFolderName(props.title, document.getElementById("folder-name-input-" + props.title).value)
      setNameSet(true)
    }
  }
  function handleDeleteClicked(event) {
    props.deleteFolderByName(props.title)
  }
  function handleOpenClicked(event) {
    props.setPosts(props.posts)
    var newFolderPath = JSON.parse(JSON.stringify(props.folderPath))
    newFolderPath.push(props.title)
    props.setFolderPath(newFolderPath)
  }
  return (
    <div className="folderItem">
      {(nameSet) ? <div onClick={handleNameClicked}> {props.title} </div> : <input type="text" id={"folder-name-input-" + props.title} style={{ 'width': "8vh" }} onKeyPress={handleNameChange} />}
      <div className="button1">
        <button className="button-30" onClick={handleOpenClicked}>Open</button>
      </div>
      <div className="button2">
        <button className="button-30" onClick={handleDeleteClicked}>Delete</button>
      </div>
    </div>
  );
}

export default FolderItem;
