
import React, { useState } from 'react';
import "./TabButton.css";

function TabButton(props) {
    const [nameSet, setNameSet] = useState(true)
    function handleXClicked(event) {
        event.stopPropagation()
        props.closeTab(props.name)
    }
    function handleNameClicked(event) {
        event.stopPropagation()
        setNameSet(false)
    }
    function handleNameChange(event) {
        if (event.key == 'Enter') {
            props.changeTabName(props.name, document.getElementById("name-input-" + props.name).value)
            setNameSet(true)
        }
    }
    return (<div className='tab-button' id={'tab-button-' + props.name} style={{ textAlign: "top","background-color": (props.selected) ? '#282828' : '#111111' }} onClick={props.onClick}>
        <div className='tab-name'> {(nameSet) ? props.name : <input type="text" id={"name-input-" + props.name} style={{ 'height': "3vh"}} onKeyPress={handleNameChange} />} </div>
        {/* <div className='rbutton' onClick={handleNameClicked} style={{ 'background-color': '#bcb8b7', 'width': "1.2vh", 'height': "1.2vh", 'font-size': 10 }} onKeyPress={handleNameChange}> </div> */}
        {props.tabs.length == 1? null : <div className='xbutton' onClick={handleXClicked} style={{ 'background-color': '#bcb8b7', 'width': "1.2vh", 'height': "1.2vh", 'font-size': 10 }}></div>}
    </div>)
}

export default TabButton;