import React, {useState, useEffect} from "react";
import SourceItem from "./SourceItem"
import './SourceList.css'
function SourceList(props) {
	const [scrollValue, setScrollValue] = [props.scrollValue, props.setScrollValue]
	useEffect(() => {console.log("FOCUS BUG EFFECT", scrollValue); document.getElementsByClassName("sourceList")[0].scrollTo({top:scrollValue})},[])
	function handleSideMenuScrolled(event)
	{
		console.log("SCROLLED IN FOCUS BUG", event.target.scrollTop)
		if(event.target.scrollTop != 0) setScrollValue(event.target.scrollTop)
	}
	return (
    		<div className="sourceList" onScroll={handleSideMenuScrolled}>
      			{props.sources.map(s => <SourceItem key={s.id} img={s.img} name={s.name} sources = {props.sources} setSources = {props.setSources} updatePosts = {props.updatePosts} color={s.color} icon={s.icon} onClick={props.onClick}/>)}
     		</div> 
  		); 
				
}

export default SourceList;