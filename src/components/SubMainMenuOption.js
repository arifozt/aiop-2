import React from "react";
import "./SubMainMenuOption.css";

function SubMainMenuOption(props) {
  return (
    <div className='subMainMenuOption' onClick={props.onClick}>
      {props.Icon}
      <h2>{props.text}</h2>
    </div>
  );
}

export default SubMainMenuOption;
