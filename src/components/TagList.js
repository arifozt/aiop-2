import React, {useState} from "react";
import TagItem from "./TagItem"
import "./TagList.css"

function TagList(props) {
	const [allTagsVisible, setAllTagsVisible] = useState(props.tags.length > 5? false: true)
	console.log(allTagsVisible)
	function handleShowMoreClicked()
	{
		setAllTagsVisible(true)
	}
	function GetTagsButton()
	{
		return(<div onClick={handleShowMoreClicked} style={{marginTop:"15vh"}} >Show More</div>)
	}
	function GetFirst5Tags()
	{
		
		return(<div className="tagList">
		{props.tags.slice(0,4).map(t => <TagItem toggleTagByName = {props.toggleTagByName} name={t.name} selected={t.selected} />)}
   </div> )
	}
	function GetAllTags()
	{
		return(<div className="tagList">
			  {props.tags.map(t => <TagItem toggleTagByName = {props.toggleTagByName} name={t.name} selected={t.selected} />)}
		 </div> )
	}
	return ( !allTagsVisible?  <div style={{display:"flex"}}> <GetFirst5Tags/> <GetTagsButton/> </div> : <GetAllTags/> ); 
				
}

export default TagList;