import React, {useState} from "react";
import "./SaveToDropDownOption.css";
import {ReactComponent as UpIcon} from '../resources/up-arrow.svg'

function SaveToDropDownOption(props) {
	function handleIconClicked(event)
	{
		event.stopPropagation()
		props.onIconClick(event)
	}
	return(
		<div className="menu-item" onClick={props.onClick}>
			{props.name}	
			{props.onIconClick == undefined? null:<UpIcon id={props.name} onClick={handleIconClicked} className="menu-item-right-icon"/>}	
      		</div>
	);
				
}

export default SaveToDropDownOption;