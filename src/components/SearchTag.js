import React from "react"
import "./SearchTag.css";

function SearchTag(props) {
	function handleXClicked(event) {
		props.toggleTagByName(props.name)
	}
	return (
		<div className="simba"> {props.name.toLowerCase()} <button className="xabutton" onClick={handleXClicked}>x</button></div>
	)
}

export default SearchTag

// style={{width:"2vh", height:"1vh", marginBottom:"2vh", backgroundColor:"white"}