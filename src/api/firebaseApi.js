import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCfRLWTa_pr4D36FCXgrJ8QBvOlh3Djk3k",
  authDomain: "scoopt-342715.firebaseapp.com",
  projectId: "scoopt-342715",
  storageBucket: "scoopt-342715.appspot.com",
  messagingSenderId: "74711676563",
  appId: "1:74711676563:web:03faf744186f22cdf77f66",
  measurementId: "G-YG0DYDFEBC"
};

// Initialize Firebase

export const firebaseApi = initializeApp(firebaseConfig);
