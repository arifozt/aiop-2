import * as $ from 'jquery';
import axios from 'axios'
import { data } from 'jquery';
import {firebaseApi} from './firebaseApi'
import {heapSort} from '../utils/Sorter'
import {getFormattedTime} from '../utils/DateHelper'
import {sourcesList} from '../constants/data.js'
const snoowrap = require('snoowrap')
export async function getUserData(props) {
firebaseApi.firestore().collection('user data requests').doc(props.username).set({"type": "get", "username": props.username, "calculating": false, "calculated": false, "loaded":false})

var unsubscribeGetUserData = (()=>{})

function applyUnsubscribeUserData() {
	unsubscribeGetUserData()
}

unsubscribeGetUserData = firebaseApi.firestore().collection('user data requests').doc(props.username).onSnapshot(data => {
						console.log("STILL ON", data["Df"], data["Df"]["sn"]["proto"]["mapValue"]["fields"])
						if(data["Df"] == null || data["Df"]["sn"] == null || data["Df"]["sn"]["proto"] == null) { 
							console.log("BUG TRACK 2")
							props.setActiveTabIndex(0)
							props.setAllTabs([{ name: 'Tab 1', selected: true, allSources: JSON.parse(JSON.stringify(Array.from(new Set(sourcesList)))), searchTags: [] }])
							props.setTabs([{ name: 'Tab 1', selected: true, allSources: JSON.parse(JSON.stringify(Array.from(new Set(sourcesList)))), searchTags: [] }])
							props.setNewTab()
							props.setAllFolders({title: "Favourites",subfolders: [],posts: [],path: []})
							props.setFolders({title: "Favourites",subfolders: [],posts: [],path: []})
							props.setLoadedUserData(true)
							unsubscribeGetUserData()
							return 
						}
						var userDataMap = data["Df"]["sn"]["proto"]["mapValue"]["fields"]
						if(userDataMap["calculated"]["booleanValue"] == false || userDataMap["calculating"]["booleanValue"] == true || Array.from(Object.keys(userDataMap)).length < 4 ) return
						console.log("STILL ON JASON BUG SOFT", userDataMap)
						var newFolders = []
						if(userDataMap["allFolders"] != null && userDataMap["allFolders"]["arrayValue"] != null && userDataMap["allFolders"]["arrayValue"]["values"] != null) {
							userDataMap["allFolders"]["arrayValue"]["values"].forEach( folder => {
								var folderMap = folder["mapValue"]["fields"]
								var parsedPath = []
								if(folderMap["path"] != null && folderMap["path"]["arrayValue"] != null && folderMap["path"]["arrayValue"]["values"] != null) {
									folderMap["path"]["arrayValue"]["values"].forEach(pathEntry => parsedPath.push(pathEntry["stringValue"]))									
								}
								
								var parsedPosts = []
								if(folderMap["posts"] != null && folderMap["posts"]["arrayValue"] != null && folderMap["posts"]["arrayValue"]["values"] != null) {console.log("CHECK BUG ", folderMap["posts"]["arrayValue"]["values"]);
									folderMap["posts"]["arrayValue"]["values"].forEach(postEntryData => {var postEntry = postEntryData["mapValue"]["fields"];console.log("CHECK BUG 2", postEntry);parsedPosts.push({comments_count: 1, date: postEntry['date']['stringValue'], image: postEntry['image'] == null? "": postEntry['image']['stringValue'], for_who: postEntry['for_who']['stringValue'], full_text: postEntry['full_text']['stringValue'], hidden: postEntry['hidden']['booleanValue'], liked: postEntry['liked']['stringValue'], saved: postEntry['saved']['booleanValue'], summary: postEntry['summary']['stringValue'], tags: [], title: postEntry['title']['stringValue'], upvote: 1, url: postEntry['url']['stringValue'], vidUrl: postEntry['vidUrl']['stringValue']}) });
								}
								var parsedName = folderMap["title"]["stringValue"]
								newFolders.push({title: parsedName, subfolders: [], path: parsedPath, posts: parsedPosts})
							})
						} 
						console.log("STILL ON JASON BUG SOFT 2", userDataMap)
						var newTabs = []
						if(userDataMap["tabs"] == null || userDataMap["tabs"]["arrayValue"] == null || userDataMap["tabs"]["arrayValue"]["values"] == null) {
							console.log("STILL ON JASON BUG", userDataMap)
							props.setActiveTabIndex(0)
							props.setAllTabs([{ name: 'Tab 1', selected: true, allSources: JSON.parse(JSON.stringify(Array.from(new Set(sourcesList)))), searchTags: [] }])
							props.setTabs([{ name: 'Tab 1', selected: true, allSources: JSON.parse(JSON.stringify(Array.from(new Set(sourcesList)))), searchTags: [] }])
							props.setNewTab()
							props.setAllFolders({title: "Favourites",subfolders: [],posts: [],path: []})
							props.setFolders({title: "Favourites",subfolders: [],posts: [],path: []})
							props.setLoadedUserData(true)
							console.log("BUG TRACK")
							console.log("UNSUBSCRIBED")
							unsubscribeGetUserData()
							return
						}
						console.log("STILL ON JASON BUG SOFT 3", userDataMap)
 userDataMap["tabs"]["arrayValue"]["values"].forEach( tab => {
							var newSources = []
							tab["mapValue"]["fields"]["allSources"]["arrayValue"]["values"].forEach(source => {						
								newSources.push({color: source["mapValue"]["fields"]["color"]["stringValue"], icon: source["mapValue"]["fields"]["icon"]["stringValue"], img: source["mapValue"]["fields"]["img"]["stringValue"], name: source["mapValue"]["fields"]["name"]["stringValue"], selected: source["mapValue"]["fields"]["selected"]["booleanValue"], sourceLink: source["mapValue"]["fields"]["sourceLink"]["stringValue"]})
							})
							newTabs.push({"allSources": newSources, "name": tab["mapValue"]["fields"]["name"]["stringValue"], "searchTags": [], "selected": tab["mapValue"]["fields"]["selected"]["booleanValue"]}) 
						})
						if(userDataMap["contacts"] != null && userDataMap["contacts"]['arrayValue'] != null && userDataMap["contacts"]['arrayValue']['values'] != null) {	
						var newContacts = []

							userDataMap["contacts"]["arrayValue"]["values"].forEach(contact => { newContacts.push({"name": contact["mapValue"]["fields"]["name"]["stringValue"]})})
							props.setAllContacts(newContacts);
						}
						console.log("STILL ON JASON BUG SOFT 4", userDataMap)
						props.setAllTabs(JSON.parse(JSON.stringify(newTabs)))
						props.setTabs(newTabs)
						var newActiveTabIndex = userDataMap["activeTabIndex"]["stringValue"] == null? userDataMap["activeTabIndex"]["integerValue"] : parseInt(userDataMap["activeTabIndex"]["stringValue"])
						if(newActiveTabIndex == null || newActiveTabIndex >= newTabs.length) newActiveTabIndex = 0
						props.setActiveTabIndex(newActiveTabIndex)
						props.setNewTab()
						console.log("STILL ON JASON BUG SOFT 5", userDataMap)
						if(newFolders.length == 0) newFolders = [{title: "Favourites",subfolders: [],posts: [],path: []}] 
					        props.setAllFolders(JSON.parse(JSON.stringify(newFolders)))
						props.setFolders(newFolders)
						console.log("ALL TABSL", newTabs)	
						props.setLoadedUserData(true)
						unsubscribeGetUserData()
						console.log("STILL ONUNSUBSCRIBED")
						firebaseApi.firestore().collection('user data requests').doc(props.username).set({loaded: true, calculated: true, calculating: false})
					})
}
export async function setUserData(props) { 
	var newTabs = JSON.parse(JSON.stringify(props.allTabs))
	newTabs[props.activeTabIndex]["allSources"] = JSON.parse(JSON.stringify(props.globalAllSources))
	newTabs[props.activeTabIndex]["searchTags"] = props.searchTags
	console.log("SOME BUG ", {"type": "set", "username": props.username, "calculating": false, "calculated": false, "loaded":false, "email": props.email, "tabs": newTabs, "activeTabIndex": props.activeTabIndex, "contacts": props.allContacts, "allFolders": props.allFolders})
	firebaseApi.firestore().collection('user data requests').doc(props.username).set({"type": "set", "username": props.username, "calculating": false, "calculated": false, "loaded":false, "email": props.email, "tabs": newTabs, "activeTabIndex": props.activeTabIndex, "contacts": props.allContacts, "allFolders": props.allFolders})
	//firebaseApi.firestore().collection('user data').doc(props.username).set(({"email": props.email, "tabs": newTabs, "activeTabIndex": props.activeTabIndex, "contacts": props.allContacts, "allFolders": props.allFolders}))
}
export async function setChatsListener(props) {

	const db = firebaseApi.firestore()
	db.collection('messages number').doc(props.username).onSnapshot(collectionSnapshot => { 
		if(collectionSnapshot["Df"] == null || collectionSnapshot["Df"]["sn"] == null || collectionSnapshot["Df"]["sn"]["proto"] == null || collectionSnapshot["Df"]["sn"]["proto"]["mapValue"] == null) return
		const messagesNoMap = collectionSnapshot["Df"]["sn"]["proto"]["mapValue"]["fields"]
		const messagesNo = messagesNoMap["messagesNo"]["integerValue"]
		const messageDifference = messagesNo - props.getMessagesNo()
		if(messageDifference > 0) {console.log("QUEUED MESSAGE AND TRIGGERED LISTENER", props.getMessagesNo(), messagesNo, messageDifference); getMessages({username: props.username, getMessages: props.getMessages, setMessages: props.setMessages, messagesLoading: props.messagesLoading, setMessagesLoading: props.setMessagesLoading, getMessagesNo: props.getMessagesNo, setMessagesNo: props.setMessagesNo, numberOfMessages: messageDifference, globalMessages: props.globalMessages, setGlobalMessages: props.setGlobalMessages})}
	})
}

export async function getMessages(props) { 
	function messageCompareByTime(a, b) {
		if(!Array.from(Object.keys(a)).includes('time')) return false
		if(!Array.from(Object.keys(b)).includes('time')) return true
		var dateA = new Date(a["time"])
		var dateB = new Date(b["time"])
		return (dateA.getTime() < dateB.getTime())
	}
		const db = firebaseApi.firestore()
		db.collection('messages get requests').doc(props.username).set({type: "get", username: props.username, calculated: false, calculating: false, loaded: false, messagesNo: props.numberOfMessages})
		var unsubscribe = db.collection('messages get requests').doc(props.username).onSnapshot(collectionSnapshot => { 
			var newMessages = []
			const requestMap = collectionSnapshot["Df"]["sn"]["proto"]["mapValue"]["fields"]
			if(requestMap["calculating"]["booleanValue"] == false && requestMap["calculated"]["booleanValue"] == true && requestMap["loaded"]["booleanValue"] == false) {
				var newMessages = []
				if(requestMap["messages"]["arrayValue"] == null || requestMap["messages"]["arrayValue"] == null) return
				requestMap["messages"]["arrayValue"]["values"].forEach(message => {newMessages.push({message: message["mapValue"]["fields"]["message"]["stringValue"], converser: message["mapValue"]["fields"]["converser"]["stringValue"], sent: JSON.parse(message["mapValue"]["fields"]["sent"]["stringValue"]), seen:  JSON.parse(message["mapValue"]["fields"]["seen"]["stringValue"]), time: message["mapValue"]["fields"]["time"]["stringValue"]})})
				console.log("SEEN CHECK ", newMessages)
				newMessages = heapSort(newMessages, messageCompareByTime)
				var newMessagesMap = JSON.parse(JSON.stringify(props.getMessages()))
				for(const message of newMessages) {
					if(!Array.from(Object.keys(newMessagesMap)).includes(message["converser"])) newMessagesMap[message["converser"]] = []
					newMessagesMap[message["converser"]].push(message)
				}
				props.setGlobalMessages(JSON.parse(JSON.stringify(newMessagesMap)))
				props.setMessages(newMessagesMap)
				props.setMessagesLoading(false)
				console.log("QUEUED MESSAGE GET LENGTH 2", props.getMessages())
				console.log("QUEUED MESSAGE GET LENGTH ", newMessages, newMessagesMap, Array.from(Object.keys(newMessagesMap)), newMessages)
				props.setMessagesNo(newMessages.length+props.getMessagesNo())
				db.collection('messages get requests').doc(props.username).set({type: "get", username: props.username, calculated: true, calculating: false, loaded: true})
				unsubscribe()
			}
		})
	
}
export async function sendMessage(props) {
	console.log("SOME BUG", props)
	const db = firebaseApi.firestore()
	const currentTime = props.time == null? getFormattedTime(): props.time
	console.log("SOME BUG", {type: "set", username: props.sender, receiver: props.receiver, calculated: false, calculating: false, loaded: false, message: props.message, time: currentTime, sent: props.sent})
	db.collection('messages set requests').doc(props.sender+currentTime).set({type: props.seenFlag == null? "set":"set seen", username: props.sender, receiver: props.receiver, calculated: false, calculating: false, loaded: false, message: props.message, time: currentTime, sent: props.sent}) 
}

export async function isUsername(props) {
	const db = firebaseApi.firestore()
	db.collection('user data').doc(props.username).get().then( snapshot => {

		return (snapshot["Df"] != null)
	})
}
export async function search_api(props) {
	var sourceNames = []
	const db = firebaseApi.firestore()	
	
	db.collection('Search Terms').doc(props.username+props.source).set({searchTerm: props.searchTerm, username:props.username, source:props.source, calculating: false, calculated: false, loaded: false})
	var unsubscribe = db.collection('Search Terms').doc(props.username+props.source).onSnapshot(docSnapshot => {
		console.log(props.source, "API CALL", docSnapshot)
		const requestMap = docSnapshot["Df"]["sn"]["proto"]["mapValue"]["fields"]
		if(requestMap["calculated"]["booleanValue"] == false || requestMap["calculating"]["booleanValue"] == true || requestMap["loaded"]["booleanValue"] == true) return
		const postList = requestMap["result"]["arrayValue"]["values"]
		var newPosts = []
		for(const post of postList) {
			const postMap = post["mapValue"]["fields"]
			newPosts.push({"date": postMap["date"]["stringValue"], "for_who": postMap["for_who"]["stringValue"], "full_text": postMap["full_text"]["stringValue"], "image": postMap["image"]["stringValue"], isVideo: postMap["isVideo"]["booleanValue"], "summary": postMap["summary"]["stringValue"], "tags": [], "title": postMap["title"]["stringValue"], "url": postMap["url"]["stringValue"], "vidUrl": postMap["vidUrl"]["stringValue"]})
		}	
		console.log(props.source, "API CALL MAP", requestMap)
		props.setPosts(newPosts)
		unsubscribe()
	})
}
export async function trending_api(props)
{
	var sourceNames = []
	const db = firebaseApi.firestore()	
	
	db.collection('Trending').doc(props.username+props.source).set({username:props.username, source:props.source, calculating: false, calculated: false, loaded: false})
	var unsubscribe = db.collection('Trending').doc(props.username+props.source).onSnapshot(docSnapshot => {
		console.log(props.source, "API CALL", docSnapshot)
		const requestMap = docSnapshot["Df"]["sn"]["proto"]["mapValue"]["fields"]
		if(requestMap["calculated"]["booleanValue"] == false || requestMap["calculating"]["booleanValue"] == true || requestMap["loaded"]["booleanValue"] == true) return
		const postList = requestMap["result"]["arrayValue"]["values"]
		var newPosts = []
		for(const post of postList) {
			const postMap = post["mapValue"]["fields"]
			newPosts.push({"date": postMap["date"]["stringValue"], "for_who": postMap["for_who"]["stringValue"], "full_text": postMap["full_text"]["stringValue"], "image": postMap["image"]["stringValue"], isVideo: postMap["isVideo"]["booleanValue"], "summary": postMap["summary"]["stringValue"], "tags": [], "title": postMap["title"]["stringValue"], "url": postMap["url"]["stringValue"], "vidUrl": postMap["vidUrl"]["stringValue"]})
		}	
		console.log(props.source, "API CALL MAP", requestMap)
		props.setPosts(newPosts)
		unsubscribe()
	})
}
export async function tiktok_api_search(props) {
	const db = firebaseApi.firestore()
	db.collection('Search Terms').doc(props.username+"tiktok").set({searchTerm: props.searchTerm, username:props.username, source:"tiktok", calculating: false, calculated: false, result: ""})
	const observer = db.collection('Search Terms').doc(props.username+"tiktok").onSnapshot(docSnapshot => {
		var allPosts = []
  		var searchResult = docSnapshot["Df"]["sn"]["proto"]["mapValue"]["fields"]
		console.log("SNAPSHOT ", searchResult)
		if(searchResult["calculated"]["booleanValue"] == false || searchResult["result"]["stringValue"] != undefined) return;
		for(var video of searchResult["result"]["mapValue"]["fields"]["item_list"]["arrayValue"]["values"]) {
			
			console.log(video, "TIKTOK SNAPSHOT")
			allPosts.push({
												upvote: video["mapValue"]["fields"]["stats"]["mapValue"]["fields"]["diggCount"]["integerValue"],
												isVideo: true,
												liked: "untouched",
												saved: false,
												hidden: false,
												for_who: "TIKTOK",
												image: video["mapValue"]["fields"]["video"]["mapValue"]["fields"]["cover"]["stringValue"],
												title: video["mapValue"]["fields"]["desc"]["stringValue"],
												summary: "",
												full_text: "",
												comments_count: video["mapValue"]["fields"]["stats"]["mapValue"]["fields"]["commentCount"]["integerValue"],
												tags: video["mapValue"]["fields"]["textExtra"] == undefined? [] : video["mapValue"]["fields"]["textExtra"]["arrayValue"]["values"].map(tagItem => tagItem["mapValue"]["fields"]["hashtagName"]["stringValue"]),
												date: video["mapValue"]["fields"]["createTime"]["integerValue"],
												url: video["mapValue"]["fields"]["video"]["mapValue"]["fields"]["playAddr"]["stringValue"],
												vidUrl: video["mapValue"]["fields"]["video"]["mapValue"]["fields"]["playAddr"]["stringValue"]
												})
		}
		console.log("SNAPSHOT POSTS", allPosts)
		props.setPosts(allPosts)
		
  // ...
}, err => {
  console.log(`Encountered error: ${err}`);
});
}
export async function youtube_api_search(props) {
	const db = firebaseApi.firestore()
	db.collection('Search Terms').doc(props.username+"youtube").set({searchTerm: props.searchTerm, username:props.username, source:"youtube", calculating: false, calculated: false, result: ""})
	const observer = db.collection('Search Terms').doc(props.username+"youtube").onSnapshot(docSnapshot => {
		if(docSnapshot["Df"]["sn"]["proto"]["mapValue"]["fields"]["calculated"]["booleanValue"] == false) return
		var allPosts = []
		var resultList = JSON.parse(docSnapshot["Df"]["sn"]["proto"]["mapValue"]["fields"]["result"]["stringValue"])["result"]
		console.log("YOUTUBE SNAPSHOT LIST", resultList)
		for(const video of resultList) {
			allPosts.push({
												upvote: 1,
												isVideo: true,
												liked: "untouched",
												saved: false,
												hidden: false,
												for_who: "YOUTUBE",
												image: video["thumbnails"][0]["url"],
												title: video["accessibility"]["title"],
												summary: "",
												full_text: "",
												comments_count: 1,
												tags: video["accessibility"]["title"].split(" ").slice(0, 8),
												date: video["publishedTime"],
												url: video["link"],
												vidUrl: video["link"].replace("watch?v=", "embed/")
												})
		}
		console.log("YOUTUBE POSTS ARE", allPosts)
		props.setPosts(allPosts)
	})

}
export async function twitter_api_search(props) {
	const db = firebaseApi.firestore()
	db.collection('Search Terms').doc(props.username+"twitter").set({searchTerm: (props.searchTerm == undefined || props.searchTerm.length == 0)? "a": props.searchTerm, username: props.username, source:"twitter", calculating: false, calculated: false, result: ""})
	const observer = db.collection('Search Terms').doc(props.username+"twitter").onSnapshot(docSnapshot => {
		var allPosts = []
  		var searchResult = docSnapshot["Df"]["sn"]["proto"]["mapValue"]["fields"]
		if(searchResult["calculated"]["booleanValue"] == false) return
		var resultList = searchResult["result"]["mapValue"]["fields"]
		console.log("TWITTER API", resultList)
		for(const resultKey of Object.keys(resultList)) {
			const result = resultList[resultKey]
			allPosts.push({upvote: result["mapValue"]["fields"]["favorite_count"]["integerValue"],
												isVideo: false,
												liked: "untouched",
												saved: false,
												hidden: false,
												for_who: "TWITTER",
												image: "https://res.cloudinary.com/demo/image/twitter/"+result["mapValue"]["fields"]["user_id_str"]["stringValue"]+".jpg",
												title: result["mapValue"]["fields"]["full_text"]["stringValue"],
												summary: result["mapValue"]["fields"]["full_text"]["stringValue"],
												full_text: result["mapValue"]["fields"]["full_text"]["stringValue"],
												comments_count: result["mapValue"]["fields"]["reply_count"]["integerValue"],
												tags: result["mapValue"]["fields"]["full_text"]["stringValue"].split(" ").slice(0, 8),
												date: result["mapValue"]["fields"]["created_at"]["stringValue"],
												url: "https://twitter.com/"+result["mapValue"]["fields"]["user_id_str"]["stringValue"]+"/status/"+result["mapValue"]["fields"]["id_str"]["stringValue"],
												vidUrl: result["mapValue"]["fields"]["id_str"]["stringValue"]
			})
		}
		props.setPosts(allPosts)
		console.log("CALCULATED RESULT POSTS FOR TWITTER IS", allPosts)
		
	})
}

export async function firebase_api_sources(props)
{
	var sourceNames = []
	const db = firebaseApi.firestore()	
	
	db.collection('news').get().then(snapshot=>{
						var snapData=[]; 
						snapshot.forEach(doc=>{snapData.push(doc.data())});
						snapData.forEach(sourceSnap => {
									var sourceName = Object.keys(sourceSnap)[0]
									sourceNames.push(sourceName)
									
								})
						console.log("TEST ", sourceNames)
						props.setSources(sourceNames)
						}).catch(error => console.log("TEST", error))
}
export async function firebase_api(props)
{
	const db = firebaseApi.firestore()
	db.collection('news').doc(props.sourceName.toLowerCase()).get().then(snapshot=>{
						var allPosts = []
						console.log("API BUG", snapshot, props.sourceName)
						if(snapshot['Df'] == null || snapshot['Df'] == undefined) {console.log("API BUG PASS");props.emptyResponseSet();return}
						const articlesMap = snapshot['Df']['sn']['proto']['mapValue']['fields'][props.sourceName.toLowerCase()]['mapValue']['fields']
											for(const articleKey of Object.keys(articlesMap)) {
												allPosts.push({
												upvote: 1,
												isVideo: false,
												liked: "untouched",
												saved: false,
												hidden: false,
												for_who: props.sourceName,
												image: articlesMap[articleKey]["mapValue"]["fields"]["url_image"]["stringValue"],
												title: articlesMap[articleKey]["mapValue"]["fields"]["title"]["stringValue"],
												summary: articlesMap[articleKey]["mapValue"]["fields"]["summary"]["stringValue"],
												full_text: articlesMap[articleKey]["mapValue"]["fields"]["text"]["stringValue"],
												comments_count: 10,
												tags: [],
												date: articlesMap[articleKey]["mapValue"]["fields"]["publish_date"]["stringValue"],
												url: articleKey,
												vidUrl: articleKey
												})
											}
											props.setPosts(allPosts)
						}).catch(error => console.log("TEST", error))
}

const redditApi = new snoowrap({
  userAgent: 'TurBooo0',
  clientId: 'MAProZbwAnwVCI8L3C51ow',
  clientSecret: 'dfO5D2n34rcuNRFmnYM81AsPxEkJ7Q',
  refreshToken: '22380690-uZhgjVYgsHO_-I_CYlLHTAER4-ZozA'
});

const {createProxyMiddleware} = require('http-proxy-middleware')
const twitchApi = axios.create({
   headers: {
     'Client-Id': '80dz9jnveoiay3udxzymy9i4z9weco',
     'Authorization' : 'Bearer np5c6248gtmkd3un8nqgadvkjatddh'
   }
 })
const wikipediaApi = axios.create({
   headers: {
   }
 })
const guardianApi = axios.create({
   headers: {
   }
 })
// var axios = require('axios');
// var data = '';

// var config = {
//   method: 'get',
//   url: 'https://api.twitter.com/2/tweets/1491481236048687105',
//   headers: { 
//     'Authorization': 'OAuth oauth_consumer_key="eyb1JBowjQpwCWWYNGGlA8aYk",oauth_token="1137002017346527232-pkgTAX12tJGfi8gcROGjZr2bITOmoC",oauth_signature_method="HMAC-SHA1",oauth_timestamp="1644522137",oauth_nonce="dosOmSG7PiL",oauth_version="1.0",oauth_body_hash="2jmj7l5rSw0yVb%2FvlWAYkK%2FYBwk%3D",oauth_signature="jjoLFsTRg%2Fkkxv4XbpRhM4zQm5I%3D"', 
//     'Cookie': 'guest_id=v1%3A164445278743792096'
//   },
//   data : data
// };

// axios(config)
// .then(function (response) {
//   console.log(JSON.stringify(response.data));
// })
// .catch(function (error) {
//   console.log(error);
// });

// var axios = require('axios');
// var data = '';

// var config = {
//   method: 'get',
//   url: 'https://api.twitter.com/2/tweets/1491828720428109824',
//   headers: { 
//     'Authorization': 'Bearer AAAAAAAAAAAAAAAAAAAAAIn1YwEAAAAAg4r28p3el5BR2cYvGHjnn3Bl0n0%3DY86parj6yJxBpgHJ3Zwxc7D9WMd3uDmyKLedRJpApYNCraXCyo', 
//     'Cookie': 'guest_id=v1%3A164445278743792096'
//   },
//   data : data
// };

// axios(config)
// .then(function (response) {
//   console.log(JSON.stringify(response.data));
// })
// .catch(function (error) {
//   console.log(error);
// });
{/**/}
export function reddit_api(props) {
	console.log("Started reddit api")
	var redditPosts = []
	redditApi.getHot().then(posts => { posts.forEach(post => redditPosts.push({
						upvote: 1,
						isVideo: post.is_video,
						liked: "untouched",
						saved: false,
						hidden: false,
						for_who: "REDDIT",
						image: post.thumbnail === undefined || !post.thumbnail.includes(".") ? "https://logos-world.net/wp-content/uploads/2020/10/Reddit-Logo.png": post.thumbnail,
						title: post.title,
						summary: "",
						full_text: post.selftext,
						comments_count: post.num_comments,
						tags: post.title.split(" ").slice(0, 8),
						date: "",
						url: post.url,
						vidUrl: post.is_video? post.media["reddit_video"]["fallback_url"]: post.url
					}))
					if(props.setPosts != undefined) props.setPosts(redditPosts)
					console.log("Ended reddit api", posts)
					})
}
export function reddit_api_search(props) {
	console.log("Started reddit api")
	var redditPosts = []
	redditApi.getSubreddit('all').search({query: props.searchTerm, sort: 'year'}).then(posts => { posts.forEach(post => redditPosts.push({
						upvote: 1,
						isVideo: post.is_video,
						liked: "untouched",
						saved: false,
						hidden: false,
						for_who: "REDDIT",
						image: post.thumbnail,
						title: post.title,
						summary: "",
						full_text: post.selftext,
						comments_count: post.num_comments,
						tags: post.title.split(" ").slice(0, 8),
						date: "",
						url: post.url,
						vidUrl: post.is_video? post.media["reddit_video"]["fallback_url"]: post.url
					}))
					if(props.setPosts != undefined) props.setPosts(redditPosts)
					console.log("Ended reddit api", posts)
					})
}
export function bbc_api(props) {

    var har = {}
    var rawFile = new XMLHttpRequest();
	rawFile.open("GET", "bbc.txt", false);
	rawFile.onreadystatechange = function () {
		if (rawFile.readyState === 4) {
			if (rawFile.status === 200 || rawFile.status === 0) {
				var allText = rawFile.responseText;
				har = JSON.parse(allText)
			}
		}
	}
    rawFile.send(null);
	var keys = Object.keys(har);
    var tempa = "a";
    var allPosts = [];
	var dat = "a"
	for (const key of keys) {
		var keys1 = Object.keys(har[key]);
		for (const k of keys1) {
			if (tempa != har[key][k]["title"]) {
				tempa = har[key][k]["title"];
				if (!har[key][k]["publish_date"]){
					dat = "2022-02-03"
				} else {
					dat = har[key][k]["publish_date"]
				}
                console.log(har[key][k]['tags'])
                if (!(har[key][k]['tags']==undefined || har[key][k]['tags'].length == 0))
				if (tempa) {
					allPosts.push({
						upvote: 1,
						isVideo: false,
						liked: "untouched",
						saved: false,
						hidden: false,
						for_who: "BBC",
						image: har[key][k]["url_image"],
						title: har[key][k]["title"],
						summary: har[key][k]["summary"],
						full_text: har[key][k]["text"],
						comments_count: 42,
						tags: har[key][k]['tags'].split(", ").slice(0, 7),
						date: dat,
						url: k
					});

				}
			}
		}
	}
    console.log(keys)
    props.setPosts(allPosts)
	console.log("Finished: ",allPosts)
	return allPosts;
}


// `https://en.wikipedia.org/w/api.php?action=quer             y&list=search&prop=info&inprop=url&utf8=&format=json&origin             =*&srlimit=20&srsearch=${trimVal}`;
//        co

	
export function wikipedia_api_search(props) {
	var posts = []
	const fetchData = async() => {
		console.log("in wiki api")
		const results = await wikipediaApi.get('https://en.wikipedia.org/w/api.php?action=query&list=search&prop=info&inprop=url&utf8=&format=json&origin=*&srlimit=20&srsearch='+props.searchTerm)
		console.log(results)
		results.data.query.search.forEach( result => posts.push({
		upvote: 1,
						isVideo: false,
                liked: "untouched",
                saved: false,
                hidden: false,
                for_who: "WIKIPEDIA",
                image: "",
                title: result.title,
                summary: result.snippet,
                full_text: "",
                comments_count: 101,
                tags: result.title.split(" ").slice(0, 8),
                date: result.timestamp,
                url: "https://en.wikipedia.org/?curid=1171211"+result.pageid,
          
		}))
        props.setPosts(posts)
	}
	fetchData()
}

export function guardian_api(props) {
	var posts = []
	const fetchData = async() => {
		console.log("in guardian api")
		const results = await guardianApi.get('https://content.guardianapis.com/search?show-blocks=all&show-fields=thumbnail&api-key=48a4952c-b1be-4e99-989d-54c0eecca964')
		console.log(results)
		results.data.response.results.forEach( result => {
		var fullText = ""
		console.log(result.blocks.body, "ARe The blocks")
		for(var body of result.blocks.body) fullText = fullText.concat(body.bodyTextSummary)
		posts.push({
		upvote: 1,
						isVideo: false,
                liked: "untouched",
                saved: false,
                hidden: false,
                for_who: "GUARDIAN",
                image: result.fields.thumbnail,
                title: result.webTitle,
                summary: result.webTitle,
                full_text: fullText,
                comments_count: 101,
                tags: fullText.split(" ").slice(0, 8),
                date: result.webPublicationDate,
                url: "https://en.wikipedia.org/?curid=1171211"+result.pageid,
          
		})})
        	props.setPosts(posts)
	}
	fetchData()
}

export function guardian_api_search(props) {
	var posts = []
	const fetchData = async() => {
		console.log("in guardian api")
		const results = await guardianApi.get('https://content.guardianapis.com/search?q='+props.searchTerm+'&show-blocks=all&show-fields=thumbnail&api-key=48a4952c-b1be-4e99-989d-54c0eecca964')
		console.log(results)
		results.data.response.results.forEach( result => {
		var fullText = ""
		console.log(result.blocks.body, "ARe The blocks")
		for(var body of result.blocks.body) fullText = fullText.concat(body.bodyTextSummary)
		posts.push({
		upvote: 1,
						isVideo: false,
                liked: "untouched",
                saved: false,
                hidden: false,
                for_who: "GUARDIAN",
                image: result.fields.thumbnail,
                title: result.webTitle,
                summary: result.webTitle,
                full_text: fullText,
                comments_count: 101,
                tags: fullText.split(" ").slice(0, 8),
                date: result.webPublicationDate,
                url: "https://en.wikipedia.org/?curid=1171211"+result.pageid,
          
		})})
        	props.setPosts(posts)
	}
	fetchData()
}


export function twitch_api(props) {
		var posts = []
	console.log(props)
	const fetchData = async() => {
		console.log("started twitch api")
		{/*const result = await axios.get('https://api.twitch.tv/kraken/streams/featured?&client_id=80dz9jnveoiay3udxzymy9i4z9weco')*/}
		const results = await twitchApi.get('https://api.twitch.tv/helix/streams?login=twitchdev')
		console.log(results.data)
		results.data.data.forEach( result => posts.push({
                upvote: 1,
						isVideo: true,
                liked: "untouched",
                saved: false,
                hidden: false,
                for_who: "TWITCH",
                image: result.thumbnail_url.replace("{width}", "100").replace("{height}", "100"),
                title: result.title,
                summary: "",
                full_text: "",
                comments_count: 101,
                tags: result.title.split(" ").slice(0, 8),
                date: result.started_at,
                url: ("https://player.twitch.tv/?channel=" + result.user_login + "&parent=localhost"),
						vidUrl: ("https://player.twitch.tv/?channel=" + result.user_login + "&parent=localhost"),
                type: "video"}))
        props.setPosts(posts)
		console.log("finshed twitch api")
	}
	fetchData()
}
export function twitch_api_search(props) {
		var posts = []
	console.log(props)
	const fetchData = async() => {
		console.log("started twitch api")
		{/*const result = await axios.get('https://api.twitch.tv/kraken/search/streams?queryfeatured?&client_id=80dz9jnveoiay3udxzymy9i4z9weco')*/}
		const results = await twitchApi.get('https://api.twitch.tv/helix/search/channels?query='+props.searchTerm)
		console.log(results.data)
		results.data.data.forEach( result => posts.push({
                upvote: 1,
						isVideo: true,
                liked: "untouched",
                saved: false,
                hidden: false,
                for_who: "TWITCH",
                image: result.thumbnail_url.replace("{width}", "100").replace("{height}", "100"),
                title: result.title,
                summary: "",
                full_text: "",
                comments_count: 101,
                tags: result.title.split(" ").slice(0, 8),
                date: result.started_at,
                url: ("https://player.twitch.tv/?channel=" + result.broadcaster_login + "&parent=localhost"),
						vidUrl: ("https://player.twitch.tv/?channel=" + result.user_login + "&parent=localhost"),
                type: "video"}))
		props.setPosts(posts)
		console.log("finshed twitch api")
	}
	fetchData()
	console.log("posts from twitch search: " , posts)
	return posts
}


// const API_URL = `https://api.spotify.com/v1/search?query=${encodeURIComponent(
// 		searchTerm
// 	)}&type=album,playlist,artist`;
// 	const result = await get(API_URL);
// 	console.log(result);
// 	const { albums, artists, playlists } = result;
// 	dispatch(setAlbums(albums));
// 	dispatch(setArtists(artists));
// 	return dispatch(setPlayList(playlists));

export function spotify_api(props) {
console.log(props)
const spotifyApi = axios.create({
	headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
		'Authorization': 'Bearer ' + props.SPOTIFY_ACCESS_TOKEN
	}
})
function addEmbedToString(inputString)
{
    var outputString = ""
    var comFlag = false
    for(var i=0;i<inputString.length;i++)
    {
        outputString += inputString[i]
        if(comFlag == false && i > 2 && inputString[i-1] == 'm' & inputString[i-2] == 'o' & inputString[i-3] == 'c')
        {
            comFlag = true
            outputString += "embed/"
        }
    }
    return outputString
}
const fetchData = async() => {
    console.log("started spotify api")
    {/*const result = await axios.get('https://api.twitch.tv/kraken/streams/featured?&client_id=80dz9jnveoiay3udxzymy9i4z9weco')*/}
    const results = await spotifyApi.get('https://api.spotify.com/v1/search?query=banana&type=track,episode')
    console.log(results.data)
    var allPosts = []
    results.data.tracks.items.forEach(track => allPosts.push(
    {
        upvote: 1,
        isVideo: true,
        liked: "untouched",
        saved: false,
        hidden: false,
        for_who: "SPOTIFY",
        image: track.album.images[0].url,
        title: track.name,
        summary: "",
        full_text: "",
        comments_count: 4,
        tags: track.name.split(" "),
        date: "",
        url: track.external_urls.spotify,
        vidUrl: addEmbedToString(track.external_urls.spotify)
    }))
    results.data.episodes.items.forEach(track => allPosts.push(
    {
        upvote: 1,
        isVideo: true,
        liked: "untouched",
        saved: false,
        hidden: false,
        for_who: "SPOTIFY",
        image: track.images[0].url,
        title: track.name,
        summary: "",
        full_text: "",
        comments_count: 4,
        tags: track.name.split(" "),
        date: "",
        url: track.external_urls.spotify,
        vidUrl: addEmbedToString(track.external_urls.spotify)
    }))
    console.log(allPosts)
    props.setPosts(allPosts)
    console.log(results.data, "finshed spotify api")
}
fetchData()
}


export function spotify_api_search(props) {
console.log(props)
const spotifyApi = axios.create({
	headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
		'Authorization': 'Bearer ' + props.SPOTIFY_ACCESS_TOKEN
	}
})
function addEmbedToString(inputString)
{
    var outputString = ""
    var comFlag = false
    for(var i=0;i<inputString.length;i++)
    {
        outputString += inputString[i]
        if(comFlag == false && i > 2 && inputString[i-1] == 'm' & inputString[i-2] == 'o' & inputString[i-3] == 'c')
        {
            comFlag = true
            outputString += "embed/"
        }
    }
    return outputString
}
const fetchData = async() => {

    console.log("started spotify api")
    {/*const result = await axios.get('https://api.twitch.tv/kraken/streams/featured?&client_id=80dz9jnveoiay3udxzymy9i4z9weco')*/}
    const results = await spotifyApi.get('https://api.spotify.com/v1/search?query='+props.searchTerm+'&type=track,episode')
    console.log(results.data)
    var allPosts = []
    results.data.tracks.items.forEach(track => allPosts.push(
    {
        upvote: 1,
        isVideo: true,
        liked: "untouched",
        saved: false,
        hidden: false,
        for_who: "SPOTIFY",
        image: track.album.images[0].url,
        title: track.name,
        summary: "",
        full_text: "",
        comments_count: 4,
        tags: track.name.split(" "),
        date: "",
        url: track.external_urls.spotify,
        vidUrl: addEmbedToString(track.external_urls.spotify)
    }))
    results.data.episodes.items.forEach(track => allPosts.push(
    {
        upvote: 1,
        isVideo: true,
        liked: "untouched",
        saved: false,
        hidden: false,
        for_who: "SPOTIFY",
        image: track.images[0].url,
        title: track.name,
        summary: "",
        full_text: "",
        comments_count: 4,
        tags: track.name.split(" "),
        date: "",
        url: track.external_urls.spotify,
        vidUrl: addEmbedToString(track.external_urls.spotify)
    }))
    console.log(allPosts)
    props.setPosts(allPosts)
    console.log("finshed spotify api")
}
fetchData()
}