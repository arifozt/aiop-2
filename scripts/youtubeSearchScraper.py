from youtubesearchpython import VideosSearch
import json

def youtubeSearchScraper(searchTerm):
    videosSearch = VideosSearch(searchTerm, limit = 10)

    return json.dumps(videosSearch.result())


