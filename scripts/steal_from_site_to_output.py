from newspaper import Article
from langdetect import detect_langs
from difflib import SequenceMatcher
import newspaper
from datetime import datetime
import sys

extraSources = ['https://www.forbes.com', 'https://www.bbc.com', 'https://www.ft.com', 'https://www.bbc.co.uk', 'https://www.reuters.com', 'https://edition.cnn.com']
# sourcesList = ['https://www.businessinsider.com', 'https://www.yahoo.com', 'https://www.dailymail.co.uk', 'https://apnews.com', 'https://www.nasdaq.com', 'https://www.cnn.com', 'https://news.mongabay.com', 'https://euobserver.com', 'https://www.euractiv.com', 'https://abcnews.go.com', 'https://www.dw.com', 'https://www.france24.com', 'https://www.cnbc.com', 'https://thehill.com', 'https://www.vice.com', 'https://news.am', 'https://www.ndtv.com', 'https://news.yahoo.com', 'https://www.ncronline.org', 'https://www.dailysabah.com', 'https://www.foxnews.com', 'https://www.jpost.com', 'https://theaviationist.com', 'https://www.riotimesonline.com', 'https://www.scmp.com', 'https://www.irishtimes.com', 'https://www.bangkokpost.com', 'https://www.channelnewsasia.com', 'https://www.usnews.com', 'https://www.npr.org', 'https://dailyhive.com', 'https://metro.co.uk', 'https://www.rferl.org', 'https://guardian.ng', 'https://www.nbcnews.com', 'https://www.nytimes.com', 'https://www.euronews.com', 'https://www.business-standard.com', 'https://appleinsider.com', 'https://www.themoscowtimes.com', 'https://www.al-monitor.com', 'https://www.haaretz.com', 'https://www.rappler.com', 'http://apnews.com', 'https://editions.garoweonline.com', 'https://www.theglobeandmail.com', 'https://www.saltwire.com', 'https://nationalpost.com', 'https://finance.yahoo.com', 'https://www.cityam.com', 'https://www.thedailybeast.com', 'https://renewablesnow.com', 'https://www.newsweek.com', 'https://www.ynetnews.com', 'http://www.algemeiner.com', 'https://mesonstars.com', 'https://bobrtimes.com', 'https://www.cbsnews.com', 'https://www.ekathimerini.com', 'https://www.thedrive.com', 'https://english.elpais.com', 'https://insiderpaper.com', 'https://www.stripes.com', 'https://www.commondreams.org', 'https://www.rte.ie', 'https://www.smithsonianmag.com', 'https://www.thenationalnews.com', 'https://www.taiwannews.com.tw', 'https://www.urdupoint.com', 'https://jamaica-gleaner.com', 'https://www.washingtonpost.com', 'https://www.telegraph.co.uk', 'https://globalvoices.org']
sourcesList = ["https://bbc.co.uk", "https://www.bbc.com", "https://www.dailymail.co.uk", "https://www.theguardian.com", "https://www.express.co.uk", "https://www.thesun.co.uk", "https://www.mirror.co.uk", "https://www.independent.co.uk", "https://www.news.sky.com", "https://www.msn.com", "https://www.telegraph.co.uk", "https://www.newsnow.co.uk", "https://www.manchestereveningnews.co.uk", "https://www.thetimes.co.uk", "https://www.hellomagazine.com", "https://www.newzit.com", "https://www.dailystar.co.uk", "https://www.metro.co.uk", "https://www.liverpoolecho.co.uk", "https://www.walesonline.co.uk", "https://www.news.google.com", "https://www.standard.co.uk", "https://www.cnn.com", "https://www.edition.cnn.com", "https://www.dailyrecord.co.uk", "https://www.birminghammail.co.uk", "https://www.inews.co.uk", "https://www.inyourarea.co.uk", "https://www.mylondon.news", "https://www.chroniclelive.co.uk", "https://www.nytimes.com", "https://www.examinerlive.co.uk", "https://www.forbes.com", "https://www.news.yahoo.com", "https://www.buzzfeed.com", "https://www.finance.yahoo.com", "https://www.nottinghampost.com", "https://www.businessinsider.com", "https://www.kentonline.co.uk", "https://www.leicestermercury.co.uk", "https://www.expressandstar.com", "https://www.bloomberg.com", "https://www.indiatimes.com", "https://www.scotsman.com", "https://www.rt.com", "https://www.reuters.com", "https://www.entertainmentdaily.co.uk", "https://www.hulldailymail.co.uk", "https://www.devonlive.com", "https://www.ndtv.com", "https://www.thetelegraphandargus.co.uk", "https://www.thescottishsun.co.uk"]
ex = []
def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()

def isNotBlank(myString):
    return bool(myString and myString.strip())

def getNewsFromLink(link, threadName):

    #str(sys.argv[1])
    links = [link]
    result = ""
    names = []
    for i in links:
        u = i.split("/")
        u1 = u[2].split(".")
        if u1[-1] != "uk":
            u1.pop()
        else:
            u1.pop()
            u1.pop()
        names.append(u1[-1])

    prev_url = ""
    tmp_dict = {}

    for i in range(len(links)):
        flag = 0
        result+=('{"'+names[i]+'": {')
        news = ""
        try:
            news = newspaper.build(links[i])
        except:
            continue
        articleCount = 0
        for art in news.articles:
            if(articleCount > 100):
                break
            try:
                article = Article(art.url)
                article.download()
                article.parse()
            except:
                continue
            tit = article.title
            tit = tit.replace('"', '')
            englishFlag = False
            try:
                if (float(str(detect_langs(tit)[0]).split(":")[1]) > 0.9) and ((str(detect_langs(tit)[0]).split(":")[0]) == "en") and (isNotBlank(tit)) and (float(similar(str(art.url), prev_url)) < float(90)):
                    englishFlag = True
            except:
                englishFlag = False
            if englishFlag:
                print("on thread" + str(threadName) + " post added : ", tit)
                prev_url = art.url
                aut = ', '.join(article.authors)
                aut = aut.replace('"', '')
                if (isNotBlank(str(article.publish_date))):
                    dat = str(article.publish_date)[:-9]
                else:
                    dat = datetime.today().strftime('%Y-%m-%d')
                im = article.top_image
                te = " ".join(article.text.split())
                te = te.replace('"', '')
                k = []
                ke = ""
                su = ""
                try:
                    article.nlp()
                    k = [key.title() for key in article.keywords]
                    ke = ', '.join(k)
                    su = " ".join(article.summary.split())
                except:
                    su=""
                su = su.replace('"', '')
                if flag == 1:
                    result+=(',"'+ art.url +'": {"authors": "'+aut+'", "title": "'+tit+'", "publish_date": "'+dat+'", "url_image": "'+im+'", "text": "'+te+'", "summary": "'+su+'", "tags": "'+ke+'"}')
                else:
                    result+=('"'+ art.url +'": {"authors": "'+aut+'", "title": "'+tit+'", "publish_date": "'+dat+'", "url_image": "'+im+'", "text": "'+te+'", "summary": "'+su+'", "tags": "'+ke+'"}')
                    flag = 1
                articleCount += 1
        result+=('}}\n')
        break
    
    return (names[0], result)
