import firebase_admin
import json
import _thread
import time


from firebase_admin import credentials
from firebase_admin import firestore
from steal_from_site_to_output import getNewsFromLink
from steal_from_site_to_output import sourcesList


THREAD_NO = 12

SERVICE_ACCOUNT_KEY = {
  "type": "service_account",
  "project_id": "aiop-5abcd",
  "private_key_id": "cbe1ec11ea9ea504c7c5d6b2c961b30a04e739fc",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDVcQxZLb6hEGGB\nk8u7LJO1DvwpcGXqIcwPukQHHCUxFVZbJ2AdzMCVGmssHgX68ymKHMHgtTAti804\nM+ZATrgrg2386tqNmd9gxszA7mhfDL1/hV90kJd0xJut7ELeQ4DQGgaacvMKv7Uj\nae0DtKSqCpXpE0Wwes/RM3Vyfs4erZ75XyYvnETfdbP4H+UJ/vPwCg1at3ItusEz\nV2aZo0GJgFYsZ7RuVBxOuOSm6ljJH2yqQ5VKPkh9zys7hylILQroqi+F9mxDOhLU\nXoAG3hP/0yOExiPfaMR3tBGv3FMcqMFPVxPBEMXbzqb33g3xI+j6VNIHUnEbBmsb\npcMcokqDAgMBAAECggEAAUyp5ZUYtwRRg6dxsxcHYy/PKzVq+WzxQBtDNMPgGhdm\nKjHoLOZFOeMqSd9469k1JIkeSQ7xVyWlvzTZrwSLrIAlWEfZwsjwCjfLoVUSP5nl\nQXR/a0dCFMzpobpxgcbpbtrptWEDx8n5AVkwvzgHVJ2x4fRjFTr6LuzgRy0sdhVX\nrA2gJNWle3cWQZahQtaFxY6HF7gDjd+8P+60l8VYUw76Cd6KNYgLxblYMw9EVVsI\njWY/+8gi7l3c3P1FA0So2cfO0lrR3rXvfHvqOYsZHm0Idb4aFenGe8dp8rbLTJeq\nZPdz2Peik72Gzrc+jX8dQ3mmEdh4iknCYplpGy14YQKBgQD0aduJrMmqPB14LXkY\n57QSpKE2FaS1dIRdKph/vj51GJWT+cNv0Sgew4+ei2GrPlvxntyrdl5hi7+rMPwp\nquJ+1ZPqXSqgs0rPNNPogEXh54S272BfBNOm5Oz5+0naNiirVGAeKOraonF5lnmc\nHB+85grOZY3YHHuDCz8kj+vGkwKBgQDfj1K5FoaxBclVqgC38ate/4IWoWVUNsf0\nm7OoUNjeJaf4MPzCUpUEv7KgUHzgeJA2HX5oNAnKZGX4HbSzj2LaEWGX0mjYDdBX\nPmbZUSJbdoZRI3kgl/Hkwa4+wtpSzDeX0gAPoXZpYROu1DTLLmmzFFhr5W5gC9L5\nzIWa9tlyUQKBgQDKQ4w9u/NL4OHWYXNUq/L2dq0+z9x1mc4sN6LqHCsdxlGKmugk\nceeMxfPllZ0nFL0MEJa3goaKnRbp+ejEJrm3ktIM4jrzsExZe3NedkDeuTS4GxMa\nryjkdZCzM0Sjw6wWVQYSD3SDqGjV4TQUf6W4MnuA4MPos3F3OehwPnY05wKBgQCy\nKeGOOgKLr4XdrEc4UPP3GH+PE37Sl3nRWgjamdvLZo0vgbLeFrxF9ZQgqSRsLAkZ\n53VnDBnFXy/YhawN8bag/XseaIQ/svqXnNaQWFWeOJ4gqbmNnSoc2bHIuLcPh0rC\nxoxf6JiasM/j46BTUKDSI+O30Teeea3gf/r2HcX7kQKBgQCTOzkjMzK4StvqvtlR\nXIrRJsqdbMtLIR/kR+uQss2zvlhPKVIzpLn57YiUtLnEYctl9xUUbc4s4RV56WjC\n8VeasuzEHJMb3udpfH0zA5K8WwFCfoWnT+IONtuHX37b7UF1fhU9vgpbm9QrFAfE\nqX6QFQNUqJ1Y4qBHBzy8GLcgVg==\n-----END PRIVATE KEY-----\n",
  "client_email": "firebase-adminsdk-kay3h@aiop-5abcd.iam.gserviceaccount.com",
  "client_id": "117492270114569252559",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-kay3h%40aiop-5abcd.iam.gserviceaccount.com"
}


def initialize_db():
    cred = credentials.Certificate(SERVICE_ACCOUNT_KEY)
    firebase_admin.initialize_app(cred)
    db = firestore.client()
    return db

def write_to_db(db, sourceTitle, content):
    contentDict = {sourceTitle:{}}
    try:
        contentDict = json.loads(content)
    except:
        pass
    db.collection('news').document(sourceTitle).set(contentDict)

def scrapeFromSites(db, threadName, linkList):
    for sourceLink in linkList:
        print("ON thread ", threadName, " trying ", sourceLink,"\n")
        newsName, newsContent = getNewsFromLink(sourceLink, threadName)
        print("Done for ", sourceLink,"\n")
        write_to_db(db, newsName, newsContent)

    
db = initialize_db()


sourcesSubList= [[0]]*(THREAD_NO+1)
BLOCK_SIZE = int(len(sourcesList)/THREAD_NO)
leftEnd = 0
rightEnd = BLOCK_SIZE
currentIndex = 0

while True:
    sourcesSubList[currentIndex] = sourcesList[leftEnd:rightEnd]
    if(rightEnd == len(sourcesList)):
        break
    leftEnd += BLOCK_SIZE
    rightEnd += BLOCK_SIZE
    currentIndex += 1
    if(rightEnd > len(sourcesList)):
        rightEnd = len(sourcesList)

try:
    for i in range(BLOCK_SIZE+1):
        if(sourcesSubList[i][0] == 0):
            continue
        _thread.start_new_thread( scrapeFromSites, (db,"Thread-"+str(i), sourcesSubList[i] ) )
        print("tried starting thread ", i,"\n")
except:
   print ("Error: unable to start thread")

    


