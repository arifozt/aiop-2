import requests
import json

#its bad practice to place your bearer token directly into the script (this is just done for illustration purposes)
BEARER_TOKEN = "Bearer AAAAAAAAAAAAAAAAAAAAAIn1YwEAAAAAg4r28p3el5BR2cYvGHjnn3Bl0n0%3DY86parj6yJxBpgHJ3Zwxc7D9WMd3uDmyKLedRJpApYNCraXCyo"
#define search twitter function
def search_twitter(query, tweet_fields):
    headers = {"Authorization": BEARER_TOKEN}

    url = "https://api.twitter.com/2/tweets/search/recent?query={}&{}".format(
        query, tweet_fields
    )
    response = requests.request("GET", url, headers=headers)

    print(response.status_code)

    if response.status_code != 200:
        raise Exception(response.status_code, response.text)
    return response.json()

def twitterSearchScraper(searchTerm):
    query = searchTerm
    if(len(query) < 3):
        query="today"
    tweet_fields = "tweet.fields=text,author_id,created_at"
    json_response = search_twitter(query=query, tweet_fields=tweet_fields)
    return json.dumps(json_response, indent=4, sort_keys=True)

