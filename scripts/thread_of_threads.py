import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import time
import json
import concurrent.futures
from PIL import Image, ImageFilter
from newspaper import Article
from langdetect import detect_langs
from difflib import SequenceMatcher
import newspaper
from datetime import datetime
from tqdm import tqdm

SERVICE_ACCOUNT_KEY = {
  "type": "service_account",
  "project_id": "aiop-5abcd",
  "private_key_id": "cbe1ec11ea9ea504c7c5d6b2c961b30a04e739fc",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDVcQxZLb6hEGGB\nk8u7LJO1DvwpcGXqIcwPukQHHCUxFVZbJ2AdzMCVGmssHgX68ymKHMHgtTAti804\nM+ZATrgrg2386tqNmd9gxszA7mhfDL1/hV90kJd0xJut7ELeQ4DQGgaacvMKv7Uj\nae0DtKSqCpXpE0Wwes/RM3Vyfs4erZ75XyYvnETfdbP4H+UJ/vPwCg1at3ItusEz\nV2aZo0GJgFYsZ7RuVBxOuOSm6ljJH2yqQ5VKPkh9zys7hylILQroqi+F9mxDOhLU\nXoAG3hP/0yOExiPfaMR3tBGv3FMcqMFPVxPBEMXbzqb33g3xI+j6VNIHUnEbBmsb\npcMcokqDAgMBAAECggEAAUyp5ZUYtwRRg6dxsxcHYy/PKzVq+WzxQBtDNMPgGhdm\nKjHoLOZFOeMqSd9469k1JIkeSQ7xVyWlvzTZrwSLrIAlWEfZwsjwCjfLoVUSP5nl\nQXR/a0dCFMzpobpxgcbpbtrptWEDx8n5AVkwvzgHVJ2x4fRjFTr6LuzgRy0sdhVX\nrA2gJNWle3cWQZahQtaFxY6HF7gDjd+8P+60l8VYUw76Cd6KNYgLxblYMw9EVVsI\njWY/+8gi7l3c3P1FA0So2cfO0lrR3rXvfHvqOYsZHm0Idb4aFenGe8dp8rbLTJeq\nZPdz2Peik72Gzrc+jX8dQ3mmEdh4iknCYplpGy14YQKBgQD0aduJrMmqPB14LXkY\n57QSpKE2FaS1dIRdKph/vj51GJWT+cNv0Sgew4+ei2GrPlvxntyrdl5hi7+rMPwp\nquJ+1ZPqXSqgs0rPNNPogEXh54S272BfBNOm5Oz5+0naNiirVGAeKOraonF5lnmc\nHB+85grOZY3YHHuDCz8kj+vGkwKBgQDfj1K5FoaxBclVqgC38ate/4IWoWVUNsf0\nm7OoUNjeJaf4MPzCUpUEv7KgUHzgeJA2HX5oNAnKZGX4HbSzj2LaEWGX0mjYDdBX\nPmbZUSJbdoZRI3kgl/Hkwa4+wtpSzDeX0gAPoXZpYROu1DTLLmmzFFhr5W5gC9L5\nzIWa9tlyUQKBgQDKQ4w9u/NL4OHWYXNUq/L2dq0+z9x1mc4sN6LqHCsdxlGKmugk\nceeMxfPllZ0nFL0MEJa3goaKnRbp+ejEJrm3ktIM4jrzsExZe3NedkDeuTS4GxMa\nryjkdZCzM0Sjw6wWVQYSD3SDqGjV4TQUf6W4MnuA4MPos3F3OehwPnY05wKBgQCy\nKeGOOgKLr4XdrEc4UPP3GH+PE37Sl3nRWgjamdvLZo0vgbLeFrxF9ZQgqSRsLAkZ\n53VnDBnFXy/YhawN8bag/XseaIQ/svqXnNaQWFWeOJ4gqbmNnSoc2bHIuLcPh0rC\nxoxf6JiasM/j46BTUKDSI+O30Teeea3gf/r2HcX7kQKBgQCTOzkjMzK4StvqvtlR\nXIrRJsqdbMtLIR/kR+uQss2zvlhPKVIzpLn57YiUtLnEYctl9xUUbc4s4RV56WjC\n8VeasuzEHJMb3udpfH0zA5K8WwFCfoWnT+IONtuHX37b7UF1fhU9vgpbm9QrFAfE\nqX6QFQNUqJ1Y4qBHBzy8GLcgVg==\n-----END PRIVATE KEY-----\n",
  "client_email": "firebase-adminsdk-kay3h@aiop-5abcd.iam.gserviceaccount.com",
  "client_id": "117492270114569252559",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-kay3h%40aiop-5abcd.iam.gserviceaccount.com"
}

t1 = time.perf_counter()
cred = credentials.Certificate(SERVICE_ACCOUNT_KEY)
firebase_admin.initialize_app(cred)
db = firestore.client()
result = {}
flag = {}
prev_url = ""


def write_to_db(db, sourceTitle, content):
    BLOCK_SIZE = 25
    contentDict = {sourceTitle:{}}
    try:
        contentDict = json.loads(content)
    except:
        print("failed json loads on write to db")
        pass
    actualSourceTitle = list(contentDict.keys())[0]
    print("ACTUAL SOURCE TITLE", actualSourceTitle)
    contentKeysList = list(contentDict[actualSourceTitle].keys())
    print("CONTEEEEEEEEEENT",contentKeysList)
    for i in range(0, len(contentKeysList), BLOCK_SIZE):
        tempDict = {actualSourceTitle:{}}
        for key in contentKeysList[i:max(len(contentKeysList),i+BLOCK_SIZE)]:
            tempDict[actualSourceTitle][key] = contentDict[actualSourceTitle][key]
        db.collection('news').document(actualSourceTitle).set(tempDict, merge=True)

def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()

def isNotBlank(myString):
    return bool(myString and myString.strip())


def retrieve_from_link(url):
    global result
    global flag
    global prev_url
    u = url.split("/")
    u1 = u[2].split(".")
    if u1[-1] != "uk":
        u1.pop()
    else:
        u1.pop()
        u1.pop()
    name = u1[-1]
    try:
        article = Article(url)
        article.download()
        article.parse()
        tit = article.title
        tit = tit.replace('"', '')
        print("FOR "+url+" TITLE: "+tit)
        englishFlag = False
        try:
            if (float(str(detect_langs(tit)[0]).split(":")[1]) > 0.9) and ((str(detect_langs(tit)[0]).split(":")[0]) == "en") and (isNotBlank(tit)) and (float(similar(str(url), prev_url)) < float(90)):
                englishFlag = True
        except:
            englishFlag = False
        if englishFlag:
            print(" POST ADDEEDDDDD : " + tit)
            prev_url = url
            aut = ', '.join(article.authors)
            aut = aut.replace('"', '')
            if (isNotBlank(str(article.publish_date))):
                dat = str(article.publish_date)[:-9]
            else:
                dat = datetime.today().strftime('%Y-%m-%d')
            im = article.top_image
            te = " ".join(article.text.split())
            te = te.replace('"', '')
            k = []
            ke = ""
            su = ""
            try:
                article.nlp()
                k = [key.title() for key in article.keywords].split(", ")
                ke = ', '.join(k)
                su = " ".join(article.summary.split())
            except:
                su=""
            su = su.replace('"', '')
            if flag[name] == 1:
                result[name]+=(',"'+ url +'": {"authors": "'+aut+'", "title": "'+tit+'", "publish_date": "'+dat+'", "url_image": "'+im+'", "text": "'+te+'", "summary": "'+su+'", "tags": "'+ke+'"}')
            else:
                result[name]+=('"'+ url +'": {"authors": "'+aut+'", "title": "'+tit+'", "publish_date": "'+dat+'", "url_image": "'+im+'", "text": "'+te+'", "summary": "'+su+'", "tags": "'+ke+'"}')
                flag[name] = 1
    except:
        print("FUCK OFF")

def getNewsFromLink(link):
    global result
    global flag
    global prev_url
    global db
    t5 = time.perf_counter()
    u = link.split("/")
    u1 = u[2].split(".")
    if u1[-1] != "uk":
        u1.pop()
    else:
        u1.pop()
        u1.pop()
    name = u1[-1]
    prev_url = ""
    flag[name] = 0
    result[name]=('{"'+name+'": {')
    news = newspaper.build(link)
    print("Thread" + str(link))
    articleCount = 0
    article_urls = []
    for art in news.articles:
        article_urls.append(art.url)
        if(articleCount > 50):
            break
        articleCount += 1
    print(article_urls)

    with concurrent.futures.ThreadPoolExecutor() as executor:
        results = list(tqdm(executor.map(retrieve_from_link, article_urls), total=len(article_urls)))
    # print(results)
    result[name]+=('}}\n')
    # print(result)
    print("Finished with "+name)
    t3 = time.perf_counter()
    print(f'Finished in {t3-t5} seconds')

    write_to_db(db, name, result[name])
    return (name, result[name])

if __name__ == '__main__':
    source_names = ["https://thenextweb.com/", "https://www.wired.com/"]
    with concurrent.futures.ThreadPoolExecutor() as executor:
        results = list(tqdm(executor.map(getNewsFromLink, source_names), total=len(source_names)))
    t2 = time.perf_counter()
    print(f'Finished in {t2-t1} seconds')
